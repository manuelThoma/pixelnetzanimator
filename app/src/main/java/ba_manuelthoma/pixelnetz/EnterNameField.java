package ba_manuelthoma.pixelnetz;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

/**
 * Created by thomama58961 on 13.12.2017.
 */

public class EnterNameField extends AppCompatActivity {

    public static final String KEY_NAME = "KEY_NAME";
    public static final String KEY_NAME_RESULT = "KEY_NAME_RESULT";

    private EditText etName;
    private Button btnExit;

    @Override
    protected void onCreate(Bundle bundle){
        Log.i(getClass().getSimpleName(),"onCreate()");
        super.onCreate(bundle);
        setContentView(R.layout.popup_activty_name);
        holeViews();
        //Intent ....
        //Intent intent = getIntent();
        //dauer = intent.getFloatExtra(KEY_DAUER, 1);
        initPopUp();
        initEvents();
    }

    private void initPopUp() {
        Log.i(getClass().getSimpleName(),"initPopUp()");
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        getWindow().setGravity(Gravity.TOP);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        int width = dm.widthPixels;
        int height = dm.heightPixels;
        getWindow().setLayout((int) (width * .8), (int) (height * .8));
    }

    private void holeViews() {
        etName = (EditText)findViewById(R.id.popUp_SelectName_et_name);
        btnExit = (Button)findViewById(R.id.popUp_SelectName_btn_exit);
    }

    private void initEvents(){
        //Todo OnClickListener mit EditTextListener austauschen!!
        btnExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.putExtra(KEY_NAME_RESULT,etName.getText().toString());
                Log.i(getClass().getSimpleName(),"onClick() -> setResult");
                setResult(Activity.RESULT_OK, intent);
                finish();
            }
        });
    }

}
