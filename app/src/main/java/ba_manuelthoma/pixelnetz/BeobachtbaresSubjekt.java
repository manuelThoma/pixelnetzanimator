package ba_manuelthoma.pixelnetz;

import android.app.Fragment;

/**
 * Created by thomama58961 on 13.11.2017.
 */

public interface BeobachtbaresSubjekt {
    public void registriereBeobachter(Beobachter beobachter);
    public void entferneFragment(Fragment fragment);
}
