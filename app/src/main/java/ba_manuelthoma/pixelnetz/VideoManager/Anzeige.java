package ba_manuelthoma.pixelnetz.VideoManager;


import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by thomama58961 on 08.12.2017.
 */

public class Anzeige implements Parcelable{

    private String name = "keinName";
    private int groesseX;
    private int groesseY;
    private int id = -1;

    public int getGroesseX() {
        return groesseX;
    }
    public void setGroesseX(int groesseX) {
        this.groesseX = groesseX;
    }

    public int getGroesseY() {
        return groesseY;
    }
    public void setGroesseY(int groesseY) {
        this.groesseY = groesseY;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }


    public Anzeige(){

    }


    protected Anzeige(Parcel in) {
        id = in.readInt();
        groesseX = in.readInt();
        groesseY = in.readInt();
        name = in.readString();

    }

    public static final Creator<Anzeige> CREATOR = new Creator<Anzeige>() {
        @Override
        public Anzeige createFromParcel(Parcel in) {
            return new Anzeige(in);
        }

        @Override
        public Anzeige[] newArray(int size) {
            return new Anzeige[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeInt(groesseX);
        parcel.writeInt(groesseY);
        parcel.writeString(name);
    }
}
