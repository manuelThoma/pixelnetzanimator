package ba_manuelthoma.pixelnetz.VideoManager;

import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.DragEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;

import java.io.File;

import ba_manuelthoma.pixelnetz.Animator.MeinFragmentEvent;
import ba_manuelthoma.pixelnetz.BeobachtbaresSubjekt;
import ba_manuelthoma.pixelnetz.Beobachter;
import ba_manuelthoma.pixelnetz.R;

/**
 * Created by thomama58961 on 11.12.2017.
 */

public class VmAnimationsGallerie extends Fragment implements View.OnDragListener,BeobachtbaresSubjekt{

    private View fragmentView;
    private AnimationAdapter mAnimationAdapter;
    private GridView listView;
    private ImageView imageView;
    private Beobachter beobachter;

    @Override
    public void onCreate(Bundle b)
    {
        super.onCreate(b);
        mAnimationAdapter = new AnimationAdapter(getActivity());
    }

    private void holeViews(){
        listView = (GridView)fragmentView.findViewById(R.id.vm_fg_animationSammlung_listView_animationen);
        imageView = (ImageView)fragmentView.findViewById(R.id.vm_fg_animationSammlung_view_delete);
    }
    private void initEvents(){
        listView.setAdapter(mAnimationAdapter);
        imageView.setOnDragListener(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle bundle)
    {
        fragmentView = inflater.inflate(R.layout.fragment_animationsammlung,container,false);
        holeViews();
        initEvents();


        return fragmentView;
    }

    @Override
    public boolean onDrag(View view, DragEvent event){
        int action = event.getAction();
        switch (action) {
            case DragEvent.ACTION_DRAG_STARTED:
                return true;
            case DragEvent.ACTION_DRAG_LOCATION:
                return true;
            case DragEvent.ACTION_DROP:
                Log.i("ON_DROP",event.getClipData().getItemAt(0).getText().toString());
                String animationname = event.getClipData().getItemAt(0).getText().toString();
                File dir = getActivity().getApplicationContext().getFilesDir();
                File file = new File(dir,animationname);
                file.delete();
                listViewAktualisieren();
                return true;
            case DragEvent.ACTION_DRAG_ENDED:
                return true;
        }
        return false;
    }

    public void addAnimationAlsView(String string){
        mAnimationAdapter.add(string);
    }

    private void listViewAktualisieren(){
        Log.i("FragmentANMGALERIE","listViewAktualisieren()");
        mAnimationAdapter = new AnimationAdapter(getActivity());
        listView.setAdapter(mAnimationAdapter);

        beobachter.aktualisieren(this, MeinFragmentEvent.FRAGMENT_AKTUALISIEREN);
    }

    @Override
    public void registriereBeobachter(Beobachter beobachter) {
        Log.i("FragmentANMGALERIE","registriereBeobachter()");
        this.beobachter = beobachter;
    }

    @Override
    public void entferneFragment(Fragment fragment) {
    }
}
