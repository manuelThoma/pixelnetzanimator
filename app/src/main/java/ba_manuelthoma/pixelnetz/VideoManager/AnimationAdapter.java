package ba_manuelthoma.pixelnetz.VideoManager;

import android.content.ClipData;
import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import ba_manuelthoma.pixelnetz.R;

/**
 * Created by thomama58961 on 13.12.2017.
 */

public class AnimationAdapter extends BaseAdapter {

    private Context mContext;
    private List<String> itemList = new ArrayList<>();

    public AnimationAdapter(Context c){
        mContext = c;
    }
    public void add(String filename){
        itemList.add(filename);
    }
    public void ItemListZuruecksetzen(){itemList.clear();}

    @Override
    public int getCount() {
        return itemList.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int position, View view, ViewGroup viewGroup) {
        TextView textView;

        if(view == null){
            textView = erstelleTextView();
            textView.setClickable(true);
        }
        else {
            textView =(TextView) view;
        }
        textView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view){
                Log.i("Adapter-File:",itemList.get(position));
                ClipData data = ClipData.newPlainText("File",itemList.get(position));
                View.DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(view);
                view.startDrag(data,shadowBuilder,view,0);
                return true;
            }
        });
        textView.setText(itemList.get(position));
        return textView;

    }

    private TextView erstelleTextView(){
        TextView textView = new TextView(mContext);
        ViewGroup.LayoutParams param = new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                (int)mContext.getResources().getDimension(R.dimen.fg_animationSammlung_view_hoehe));
        textView.setLayoutParams(param);
        textView.setPadding(10,10,10,10);
        textView.setTypeface(Typeface.DEFAULT_BOLD);
        textView.setTextSize((int)mContext.getResources().getDimension(R.dimen.text_size_klein));
        textView.setBackgroundColor((int)mContext.getResources().getColor(R.color.colorDarkRed));
        textView.setGravity(Gravity.CENTER);
        return textView;
    }
}