package ba_manuelthoma.pixelnetz.VideoManager;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import java.util.ArrayList;

import ba_manuelthoma.pixelnetz.R;

/**
 * Created by thomama58961 on 08.12.2017.
 */

public class VmAnzeigeParam extends AppCompatActivity implements View.OnClickListener{


    public static final String KEY_ANZEIGEN = "KEY_ANZEIGEN";
    public static final String KEY_ANZEIGEN_RESULT = "KEY_ANZEIGEN_RESULT";

    //ID "ADRESSEN" MAX ANZEIGEN ANZAHL = 10000
    private final int EDITTEXT_NAME_ID = 10000;
    private final int EDITTEXT_X_ID = 20000;
    private final int EDITTEXT_Y_ID = 30000;
    private final int BUTTON_ID = 40000;

    private final int MAX_ANZAHL_ANZEIGEN = 10;

    //Views
    private EditText etAnzahlAnzeigen;
    private Button btnAnzahlAnzeigen;
    private Button btnExit;
    private LinearLayout llyAnzeigen;

    //Daten
    private ArrayList<Anzeige> anzeigen;
    private int anzahlAnzeigen;

    @Override
    protected void onCreate(Bundle bundle){
        Log.i(getClass().getSimpleName(),"onCreate()");
        super.onCreate(bundle);
        setContentView(R.layout.popup_activity_vm_anzeigeparam);
        holeViews();
        //Intent ....
        Intent intent = getIntent();
        anzeigen = intent.getParcelableArrayListExtra(KEY_ANZEIGEN);
        Log.i(getClass().getSimpleName(),"Anzeigen#size->"+anzeigen.size());
        etAnzahlAnzeigen.setText(""+anzeigen.size());
        initAnzeigen();
        initPopUp();
        initEvents();
    }

    private void initAnzeigen(){

        if(((LinearLayout) llyAnzeigen).getChildCount() > 0)
            ((LinearLayout) llyAnzeigen).removeAllViews();

        for(int id = 0; id<anzeigen.size();id++){
            erstelleAnzeigeLayout(id);
        }

        for(int id = 0; id<anzeigen.size();id++){

            EditText et = (EditText) findViewById(EDITTEXT_NAME_ID +id);
            et.setText(anzeigen.get(id).getName());
            et = (EditText)findViewById(EDITTEXT_X_ID+id);
            et.setText(""+anzeigen.get(id).getGroesseX());
            et = (EditText)findViewById(EDITTEXT_Y_ID+id);
            et.setText(""+anzeigen.get(id).getGroesseY());
        }
    }


    private void initPopUp() {
        Log.i(getClass().getSimpleName(),"initPopUp()");
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        getWindow().setGravity(Gravity.TOP);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        int width = dm.widthPixels;
        int height = dm.heightPixels;
        getWindow().setLayout((int) (width * .9), (int) (height * .9));
    }

    private void holeViews() {
        btnExit = (Button)findViewById(R.id.vm_popUp_anzeigparam_btnExit);
        etAnzahlAnzeigen = (EditText)findViewById(R.id.vm_popUp_anzeigparam_etAnzahlAnzeigen);
        btnAnzahlAnzeigen = (Button)findViewById(R.id.vm_popUp_anzeigparam_btnAnzahlAnzeigen);
        llyAnzeigen = (LinearLayout)findViewById(R.id.vm_popUp_anzeigparam_layout);

    }
    private void initEvents(){
        btnExit.setOnClickListener(this);
        btnAnzahlAnzeigen.setOnClickListener(this);
    }

    private void erstelleAnzeigen(int anzahl){
        //Remove alte Anzeigen aus Liste und Layout
        anzeigen.clear();
        if(((LinearLayout) llyAnzeigen).getChildCount() > 0)
        ((LinearLayout) llyAnzeigen).removeAllViews();

        for(int id = 0; id<anzahl;id++){
            anzeigen.add(new Anzeige());
            erstelleAnzeigeLayout(id);
        }Log.i(getClass().getSimpleName(),"erstelleAnzeigen()  List.size->"+anzahl);
    }
    private void erstelleAnzeigeLayout(int id){
        // get id etX/etY/btn -> setID "etX+"id"/etY+"id"/btn+"id"
        LinearLayout lly = (LinearLayout)getLayoutInflater().inflate
                (R.layout.view_popup_activity_vm_anzeigeparam_anzeige,llyAnzeigen);

        EditText etName = (EditText)findViewById(R.id.vm_view_popUp_anzeigparam_et_name);
        etName.setId(EDITTEXT_NAME_ID + id);

        EditText etGroesseX = (EditText)findViewById(R.id.vm_view_popUp_anzeigparam_etX);
        etGroesseX.setId(EDITTEXT_X_ID + id);

        EditText etGroesseY = (EditText)findViewById(R.id.vm_view_popUp_anzeigparam_etY);
        etGroesseY.setId(EDITTEXT_Y_ID + id);

        Button btnAnzeige = (Button)findViewById(R.id.vm_view_popUp_anzeigparam_btn);
        btnAnzeige.setId(BUTTON_ID + id);
        btnAnzeige.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                //hole mir die zugeöhrige anzeige der jeweiligen anzeigen spalte
                Anzeige anzeige = anzeigen.get((view.getId()-BUTTON_ID));

                EditText text = (EditText) findViewById(EDITTEXT_NAME_ID+(view.getId()-BUTTON_ID));
                anzeige.setName(text.getText().toString());

                Log.i("btnAnzeigeListener","Btn von Anzeige "+(view.getId()-BUTTON_ID)+" wurde gedrückt");
                text = (EditText) findViewById(EDITTEXT_X_ID+(view.getId()-BUTTON_ID));
                try {
                    Integer.parseInt(text.getText().toString());
                    anzeige.setGroesseX(Integer.parseInt(text.getText().toString()));
                    Log.i("btnAnzeigeListener","X =  "+text.getText()+"!!!");
                }catch (Exception e){
                    Log.e("ERROR",e.getMessage());
                    anzeige.setGroesseX(0);
                }

                text = (EditText) findViewById(EDITTEXT_Y_ID+(view.getId()-BUTTON_ID));
                try {
                    Integer.parseInt(text.getText().toString());
                    anzeige.setGroesseY(Integer.parseInt(text.getText().toString()));
                    Log.i("btnAnzeigeListener","Y =  "+text.getText()+"!!!");
                }catch (Exception e){
                    Log.e("ERROR",e.getMessage());
                    anzeige.setGroesseY(0);
                }
            }
        });

        Log.i("ID","getID->AnzeigeID:"+id+"/NameID"+etName.getId());
        Log.i("ID","getID->AnzeigeID:"+id+"/XPos-ID"+etGroesseX.getId());
        Log.i("ID","getID->AnzeigeID:"+id+"/YPos-ID"+etGroesseY.getId());
    }


    @Override
    public void onClick(View view) {
        if(view==btnExit){
            Intent intent = new Intent();
            intent.putParcelableArrayListExtra(KEY_ANZEIGEN_RESULT,anzeigen);
            Log.i(getClass().getSimpleName(),"onClick() -> setResult");
            setResult(Activity.RESULT_OK, intent);
            finish();
        }
        if(view==btnAnzahlAnzeigen){
            int eingabe = Integer.parseInt(etAnzahlAnzeigen.getText().toString());
            if(eingabe>MAX_ANZAHL_ANZEIGEN){
                anzahlAnzeigen = MAX_ANZAHL_ANZEIGEN;
                etAnzahlAnzeigen.setText(""+MAX_ANZAHL_ANZEIGEN);
            }else{
                anzahlAnzeigen = eingabe;
            }
            erstelleAnzeigen(anzahlAnzeigen);
            Log.i(getClass().getSimpleName(),"onClick() anzahlAnzeigen-> "+anzahlAnzeigen);
        }
    }

}
