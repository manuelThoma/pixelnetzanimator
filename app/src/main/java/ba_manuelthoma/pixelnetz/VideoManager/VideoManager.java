package ba_manuelthoma.pixelnetz.VideoManager;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.student.pixelnetz.IPixelNetz;
import com.example.student.pixelnetz.Steuereinheit.Pixelnetz;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import ba_manuelthoma.pixelnetz.Animation.MyAnimation;
import ba_manuelthoma.pixelnetz.Animation.MyAnimationSegment;
import ba_manuelthoma.pixelnetz.Animation.SegmentBitmap;
import ba_manuelthoma.pixelnetz.Animator.MeinFragmentEvent;
import ba_manuelthoma.pixelnetz.BeobachtbaresSubjekt;
import ba_manuelthoma.pixelnetz.Beobachter;
import ba_manuelthoma.pixelnetz.R;

/**
 * Created by thomama58961 on 06.12.2017.
 */

public class VideoManager extends AppCompatActivity implements View.OnClickListener,Observer,Beobachter{

    private final static int REQUEST_CODE_SPURAUSWAHL = 4333;
    private final static int REQUEST_CODE_ANZEIGEPARAM = 4777;
    private final int SEC_IN_MSEC = 1000;
    private final int OFFSET = 2000; //Offset wann Animation nach senden gestartet werden soll in ms
    //TODO mehrere Anzeigen API technisch noch nicht möglich(Server-Clientverbindung)
    private final int ANZEIGE_ID = 0;

    //Pixelnetz API
    private IPixelNetz pixelNetz;
    private ImageView imgvReferenzAnzeige[];

    //View
    private LinearLayout lly_anzeigeparam;
    private Button btnSend;
    private LinearLayout lly_anzeigen;

    private VmAnimationsGallerie fgVmAnimationsGallerie;

    //Daten
    private ArrayList<Anzeige> anzeigen = new ArrayList<>();
    private ArrayList<VmFragmentAnzeige> fgAnzeigen = new ArrayList<>();
    private List<String> animationFiles = new ArrayList<>();

    @Override
    protected void onCreate(Bundle bundle){
        Log.i(getClass().getSimpleName(),"onCreate()");
        super.onCreate(bundle);
        setContentView(R.layout.activity_videomanager);

        // Muss gemacht werden, da es sonst nen Fehler gibt: Network in main Thread not allowed
        if (android.os.Build.VERSION.SDK_INT > 9)
        {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        holeViews();
        initEvents();

        //showFilesLogger();
        erstelleAnimationsListe();
        erstelleAnimationsGallerie();


    }
    private void holeViews(){
        lly_anzeigeparam = (LinearLayout)findViewById(R.id.vm_act_VideoManager_lly_anzeigparam);
        btnSend = (Button)findViewById(R.id.vm_act_VideoManager_btn_senden);
        lly_anzeigen = (LinearLayout)findViewById(R.id.vm_act_VideoManager_lly_anzeigen);
        fgVmAnimationsGallerie = (VmAnimationsGallerie) getFragmentManager().
              findFragmentById(R.id.vm_act_VideoManager_fg_AnimationsGallerie);

        fgVmAnimationsGallerie.registriereBeobachter(this);
        // Referenzanzeige
        //imgvReferenzAnzeige = new ImageView[1];
        //imgvReferenzAnzeige[0] = (ImageView) findViewById(R.id.imgvReferenzAnzeige1);
    }
    private void initEvents(){
        lly_anzeigeparam.setOnClickListener(this);
        btnSend.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if(view == lly_anzeigeparam){
            Intent intent = new Intent(this,VmAnzeigeParam.class);
            intent.putParcelableArrayListExtra(VmAnzeigeParam.KEY_ANZEIGEN,anzeigen);
            Log.i(getClass().getSimpleName(),"onClick() AnzeigenParam List.size->"+anzeigen.size());
            startActivityForResult(intent,REQUEST_CODE_ANZEIGEPARAM);
        }
        if(view == btnSend) {
            //TODO AniamtionsGallerie mit RefAnzeige Austauschen
            animationLaden();
            animationSenden();
            Toast.makeText(this, "Animation gesendet", Toast.LENGTH_SHORT).show();
        }
    }

    private void animationLaden(){

        MyAnimation animation;
        int aktiveSpur;

        //Mehrere Anzeigen nochnicht Implementiert (nicht sendbar)
        //for(int anzeigeID = 0; anzeigeID<anzeigen.size(); anzeigeID++)
        //pixelNetz.aktuelleAnimationZuruecksetzen();
        for(int index = 0; index<fgAnzeigen.get(ANZEIGE_ID).getAnzahlAnimationen();index++) {
            animation = fgAnzeigen.get(ANZEIGE_ID).holeAnimationsblock(index).getAnimation(); //hole Animation
            aktiveSpur = fgAnzeigen.get(ANZEIGE_ID).holeAnimationsblock(index).getAktiveSpur();//hole die gewählte Spur der Animation

            //TODO testen ob SegmenteReihenfolge wirklich korrekt ist
            for(MyAnimationSegment segment: animation.getSpuren().get(aktiveSpur).getSegments()) {
                pixelNetz.anzeigen(((SegmentBitmap) segment).getBitmap(),
                        (int)(segment.getDauer()*SEC_IN_MSEC),ANZEIGE_ID);
            }
        }

    }
    private void animationSenden(){

        pixelNetz.starten(OFFSET);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if(requestCode == REQUEST_CODE_ANZEIGEPARAM && resultCode == Activity.RESULT_OK) {
            Log.i(getClass().getSimpleName(),"onActivityResult()");
            anzeigen = intent.getParcelableArrayListExtra(VmAnzeigeParam.KEY_ANZEIGEN_RESULT);
            erstelleAnzeigen();
            erstellePixelnetz();
        }if(requestCode == REQUEST_CODE_SPURAUSWAHL && resultCode == Activity.RESULT_OK) {

        }
    }
    private void erstelleAnzeigen(){

        if(((LinearLayout) lly_anzeigen).getChildCount() > 0)
            ((LinearLayout) lly_anzeigen).removeAllViews();

        if(((LinearLayout) lly_anzeigeparam).getChildCount() > 0)
            ((LinearLayout) lly_anzeigeparam).removeAllViews();

        if(anzeigen.size() == 0){
            //TODO erstelle TEXTVIEW MIT text -> "Anzeige Erstellen"
        }else {
            fgAnzeigen.clear();
            for (int i = 0; i < anzeigen.size(); i++) {
                VmFragmentAnzeige vmFragmentAnzeige = VmFragmentAnzeige.erstelleVmFragmentAnzeige(anzeigen.get(i));
                getFragmentManager().beginTransaction().add(lly_anzeigen.getId(),
                        vmFragmentAnzeige, "tag").commit();
                updateLayoutAnzeigenParam(anzeigen.get(i).getName(),
                        anzeigen.get(i).getGroesseX(), anzeigen.get(i).getGroesseY());
                fgAnzeigen.add(vmFragmentAnzeige);
            }
        }
    }
    private void updateLayoutAnzeigenParam(String name, int groesseX, int groesseY){

        LinearLayout container = (LinearLayout)getLayoutInflater().inflate
                (R.layout.view_vm_anzeigeparam_layout,new LinearLayout(this));

        ((TextView) container.findViewById(R.id.vm_view_anzeigeparamLayout_tv_name)).setText(name);
        ((TextView) container.findViewById(R.id.vm_view_anzeigeparamLayout_tv_X)).setText(""+groesseX);
        ((TextView) container.findViewById(R.id.vm_view_anzeigeparamLayout_tv_Y)).setText(""+groesseY);

        lly_anzeigeparam.addView(container);
    }

    /**private void showFilesLogger(){
        String[] string = fileList();

        StringBuilder builder = new StringBuilder();
        for(String s : string) {
            builder.append("\n");
            builder.append(s);
        }
        String str = builder.toString();
        Log.i(getClass().getSimpleName(),"STORAGE FILES:-> "+str);
    }*/


    private void erstelleAnimationsListe(){
        animationFiles.clear();
        Log.i("VM","erstelleAnimationsListe(): Files: " + animationFiles.toString());
        File[] files = getFilesDir().listFiles();
        for (File file : files) {
            if (!file.isDirectory()) {
                if (file.getName().endsWith(".amn")) {
                    animationFiles.add(file.getName());
                }
            }
        }

        Log.i("FILE_LIST",animationFiles.toString());
    }

    private void erstelleAnimationsGallerie(){

        for(String s : animationFiles) {
            fgVmAnimationsGallerie.addAnimationAlsView(s);
        }
    }
    // Ab hier nur mit Absprache von David was aendern  ;D
    private void erstellePixelnetz(){
        Log.d("VideoManager","erstellePixelnetz");
        pixelNetz = new Pixelnetz(anzeigen.size());
        pixelNetz.observerHinzufuegen(this);

       for(int i = 0; i<anzeigen.size(); i++){
            pixelNetz.erstellen(anzeigen.get(i).getGroesseX(),anzeigen.get(i).getGroesseY(),10000+i,i);
       }
    }

    @Override
    public void update(Observable observable, Object o) {
        Log.d("VideoManager","notified");

        // Empfangen von 2 Objects. Bitmap und ReferenzanzeigenID
        Object[] uebergabeWert = new Object[2];
        uebergabeWert = (Object[])o;
        final Bitmap bitmap = (Bitmap)uebergabeWert[0];
        int referenzAnzeigenID = (Integer)uebergabeWert[1];

        // Pruefen welche ReferenzAnzeige notified hat
        for(int i = 0; i<anzeigen.size(); i++){
            if(i == referenzAnzeigenID){
                final int finalI = i;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //imgvReferenzAnzeige[finalI].setImageBitmap(bitmap);
                    }
                });
            }
        }
    }

    @Override
    public void aktualisieren(BeobachtbaresSubjekt b, MeinFragmentEvent event) {
        if(event == MeinFragmentEvent.FRAGMENT_AKTUALISIEREN){
            Log.i("VM","aktualisieren()");
            erstelleAnimationsListe();
            erstelleAnimationsGallerie();
        }
    }
}
