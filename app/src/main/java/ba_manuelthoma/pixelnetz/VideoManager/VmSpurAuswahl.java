package ba_manuelthoma.pixelnetz.VideoManager;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.widget.ListView;

import ba_manuelthoma.pixelnetz.Animation.MyAnimation;
import ba_manuelthoma.pixelnetz.R;
/**
 * Created by thomama58961 on 08.12.2017.
 */

public class VmSpurAuswahl extends AppCompatActivity {

    public static final String KEY_SPUR = "KEY_SPUR";
    public static final String KEY_SPUR_RESULT = "KEY_SPUR_RESULT";


    private VmSpurAdapter mSpurAdapter;
    private ListView listView;

    private MyAnimation animation;


    @Override
    protected void onCreate(Bundle bundle){
        Log.i(getClass().getSimpleName(),"onCreate()");
        super.onCreate(bundle);
        setContentView(R.layout.popup_activity_vm_spurauswahl);
        mSpurAdapter = new VmSpurAdapter(this);
        holeViews();
        //Intent ....
        Intent intent = getIntent();
        animation = intent.getParcelableExtra(KEY_SPUR);
        initPopUp();
        initEvents();
    }

    private void initPopUp() {
        Log.i(getClass().getSimpleName(),"initPopUp()");
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        getWindow().setGravity(Gravity.TOP);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        int width = dm.widthPixels;
        int height = dm.heightPixels;
        getWindow().setLayout((int) (width * .8), (int) (height * .8));
    }

    private void holeViews() {
        listView = (ListView)findViewById(R.id.vm_fg_SpurAuswahl_listView_spuren);
    }

    private void initEvents(){
        listView.setAdapter(mSpurAdapter);
        for(int i = 0; i< animation.getSpuren().size(); i++) {
            mSpurAdapter.add(animation.getSpuren().get(i));
        }
    }

    public void ClosePopUp(int spurID){
        //TODO
        Intent intent = new Intent();
        intent.putExtra(KEY_SPUR_RESULT, spurID);
        setResult(Activity.RESULT_OK, intent);
        finish();
    }


}
