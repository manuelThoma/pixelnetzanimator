package ba_manuelthoma.pixelnetz.VideoManager;

import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.DragEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import ba_manuelthoma.pixelnetz.Animator.MeinFragmentEvent;
import ba_manuelthoma.pixelnetz.BeobachtbaresSubjekt;
import ba_manuelthoma.pixelnetz.Beobachter;
import ba_manuelthoma.pixelnetz.R;

/**
 * Created by thomama58961 on 11.12.2017.
 */

public class VmFragmentAnzeige extends Fragment implements View.OnDragListener,Beobachter{

    private final static String KEY_ANZEIGE = "KEY_ANZEIGE";

    private View fragmentView;
    private TextView tvAnzeige;
    private Anzeige anzeige;
    private LinearLayout lly_anzeige;
    private static int helpCounter = 1;

    private List<VmFragmentAnimation> animationsBloecke = new ArrayList<>();

    public void onCreate(Bundle b)
    {
        Log.i(getClass().getSimpleName(),"onCreate()");
        super.onCreate(b);
        Bundle args = getArguments();
        if(args != null){
            anzeige = args.getParcelable(KEY_ANZEIGE);
            Log.i(getClass().getSimpleName(),"Anzeige: X/Y ->"+anzeige.getGroesseX()+"/"+anzeige.getGroesseY());
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle bundle)
    {
        Log.i(getClass().getSimpleName(),"onCreateView()");
        fragmentView = (ViewGroup) inflater.inflate(R.layout.fragment_anzeige,container,false);
        holeViews();
        setzeNamePlate();
        initEvents();
        return fragmentView;
    }

    private void holeViews() {
        tvAnzeige = (TextView)fragmentView.findViewById(R.id.vm_fg_FragmentAnzeige_tv_name);
        lly_anzeige = (LinearLayout)fragmentView.findViewById(R.id.vm_fg_FragmentAnzeige_lly_anzeige);
    }
    private void setzeNamePlate(){
        tvAnzeige.setText(anzeige.getName()+"\t"+anzeige.getGroesseX()+"/"+anzeige.getGroesseY());

    }

    private void initEvents()
    {
        lly_anzeige.setOnDragListener(this);
    }

    public static VmFragmentAnzeige erstelleVmFragmentAnzeige(Anzeige anzeige){
        Log.i("erzeugeFragment","static VmFragmentAnzeige erstelleVmFragmentAnzeige()");
        VmFragmentAnzeige fragment = new VmFragmentAnzeige();
        Bundle b = new Bundle();
        b.putParcelable(KEY_ANZEIGE,anzeige);
        fragment.setArguments(b);
        return fragment;
    }

    @Override
    public boolean onDrag(View view, DragEvent dragEvent) {
        int action = dragEvent.getAction();
        // Handles each of the expected events
        switch (action) {
            case DragEvent.ACTION_DRAG_STARTED:
                return true;
            case DragEvent.ACTION_DRAG_LOCATION:
                return true;
            case DragEvent.ACTION_DROP:
                //TODO
                String fileName = dragEvent.getClipData().getItemAt(0).getText().toString();
                int pos = setzeMeineIndexPos(view,dragEvent);
                Log.i(getClass().getSimpleName(),"onDrag-> Layout->"+view.toString());
                erstelleFragment(fileName,pos);
                return true;
            case DragEvent.ACTION_DRAG_ENDED:
                return true;
        }
        return false;
    }
    private int setzeMeineIndexPos(View layout,DragEvent element) {
        int letztePos = ((ViewGroup)layout).getChildCount();
        //hole alle Elemente aus dem Layout
        for(int index=0; index<((ViewGroup)layout).getChildCount(); ++index) {
            //gib mit Child mit dem niedrigsten index
            View nextChild = ((ViewGroup)layout).getChildAt(index);
            //wenn drop Pos von drawIv < Mitte von ChildIv dann setze ChildIv
            if(element.getX() < (nextChild.getX()+nextChild.getWidth()/2)) {
                return index;
            }
        }
        return letztePos;
    }

    private void erstelleFragment(String fileAnimation,int position) {

        LinearLayout container = new LinearLayout(getActivity().getApplicationContext());
        container.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT));
        container.setId(1000+helpCounter++);
        //TODO containerID dynamisch setzen evtl ID checken ob es da keine probleme gibt

        VmFragmentAnimation fgAnimation = VmFragmentAnimation.erstelleSegment(fileAnimation);
        getFragmentManager().beginTransaction().add(container.getId(),
                fgAnimation, "tag").commit();
        fgAnimation.registriereBeobachter(this);
        animationsBloecke.add(fgAnimation);
        lly_anzeige.addView(container,position);

    }

    //liste in korrekter reihenfolge holen
    public VmFragmentAnimation holeAnimationsblock(int index){
        Log.i("FRAGMENT_ANZEIGE:","holeAnimationsblock()->INDEXID"+index);
        int fgID = lly_anzeige.getChildAt(index).getId();
        for(VmFragmentAnimation fragment : animationsBloecke){
            Log.i("FRAGMENT_ANZEIGE:","holeAnimationsblock()->FragmentID"+fragment.getId());
            if(fgID == fragment.getId()){
                return fragment;
            }
        }
        Log.e("RETURN FEHLER","VmFragmentAnzeige:holeAnimationsblock(): fragment nicht gefunden");
        return null;
    }

    public int getAnzahlAnimationen(){
        return animationsBloecke.size();
    }

    @Override
    public void aktualisieren(BeobachtbaresSubjekt b, MeinFragmentEvent event) {
        if(event == MeinFragmentEvent.FRAGMENT_ENTFERNEN) {
            getFragmentManager().beginTransaction().remove((Fragment) b).commit();
            View view = fragmentView.findViewById(((Fragment) b).getId());
            lly_anzeige.removeView(view);
        }
    }
}
