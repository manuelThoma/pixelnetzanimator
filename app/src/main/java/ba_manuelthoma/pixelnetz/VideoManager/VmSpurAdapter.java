package ba_manuelthoma.pixelnetz.VideoManager;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import ba_manuelthoma.pixelnetz.Animation.MyAnimationSpur;
import ba_manuelthoma.pixelnetz.R;
import ba_manuelthoma.pixelnetz.VideoManager.VmSpurAuswahl;

/**
 * Created by thomama58961 on 13.12.2017.
 */

public class VmSpurAdapter extends BaseAdapter{

    private Context mContext;
    private List<MyAnimationSpur> itemList = new ArrayList<>();

    public VmSpurAdapter(Context c){
        mContext = c;
    }
    public void add(MyAnimationSpur spur){
        itemList.add(spur);
    }

    @Override
    public int getCount() {
        return itemList.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }


    @Override
    public View getView(final int position, View view, ViewGroup viewGroup) {
        LinearLayout container = new LinearLayout(mContext);
        LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                (int)mContext.getResources().getDimension(R.dimen.fg_animationSammlung_view_hoehe));
        container.setLayoutParams(param);

        TextView textView1;
        textView1 = erstelleTextView(itemList.get(position).getName(),position);

        TextView textView2;
        textView2 = erstelleTextView(itemList.get(position).getSpurDauer()+" sek",position);

        container.addView(textView1);
        container.addView(textView2);
        container.setBackgroundColor((int)mContext.getResources().getColor(R.color.colorLightBlue));

        return container;
    }


    private TextView erstelleTextView(final String text, final int position){
        TextView textView = new TextView(mContext);
        LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.MATCH_PARENT);
        param.weight = 1;
        textView.setLayoutParams(param);
        textView.setPadding(10,10,10,10);
        textView.setTextSize((int)mContext.getResources().getDimension(R.dimen.text_size_klein));
        textView.setText(text);
        textView.setGravity(Gravity.CENTER);
        textView.setClickable(true);
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //int action = event.getAction();
                if(mContext instanceof VmSpurAuswahl){
                    ((VmSpurAuswahl)mContext).ClosePopUp(position);
                }
            }
        });
        textView.setBackgroundColor((int)mContext.getResources().getColor(R.color.transparent));

        return textView;
    }
}
