package ba_manuelthoma.pixelnetz.VideoManager;


import android.app.Fragment;
import android.content.ClipData;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import ba_manuelthoma.pixelnetz.Animation.MyAnimation;
import ba_manuelthoma.pixelnetz.Animator.MeinFragmentEvent;
import ba_manuelthoma.pixelnetz.BeobachtbaresSubjekt;
import ba_manuelthoma.pixelnetz.Beobachter;
import ba_manuelthoma.pixelnetz.Memory.IMemoryAmnLaden;
import ba_manuelthoma.pixelnetz.Memory.MemoryIntern;
import ba_manuelthoma.pixelnetz.R;

import static android.app.Activity.RESULT_OK;


/**
 * Created by thomama58961 on 11.12.2017.
 */

public class VmFragmentAnimation extends Fragment implements View.OnClickListener,View.OnLongClickListener,BeobachtbaresSubjekt {


    private static final String KEY_ANIMATION = "KEY_ANIMATION";
    private final static int REQUEST_CODE = 2875;

    private final int SPUR_INIT_DEFAULT_VALUE = 0;

    private TextView amnName;
    private TextView amnDauer;
    private TextView amnSpur;


    private TextView tvanimation;

    private MyAnimation animation;
    private int aktiveSpur = SPUR_INIT_DEFAULT_VALUE;
    private String fileAnimation;
    private View fragmentView;


    private Beobachter beobachter;

    private IMemoryAmnLaden memory = new MemoryIntern();

    @Override
    public void onCreate(Bundle b){
        super.onCreate(b);
        Bundle args = getArguments();
        fileAnimation = args.getString(KEY_ANIMATION);
        if(args != null)
        {
            animation = memory.ladeAnimation(getActivity().getApplicationContext(),fileAnimation);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle bundle)
    {
        Log.i(getClass().getSimpleName(),"onCreateView()");
        fragmentView = (ViewGroup) inflater.inflate(R.layout.fragment_animation,container,false);
        holeViews();
        initEvents();
        updateViews(SPUR_INIT_DEFAULT_VALUE);
        return fragmentView;
    }
    private void holeViews(){
        amnName = (TextView)fragmentView.findViewById(R.id.vm_fg_FragmentAnimation_tv_amnName);
        amnDauer = (TextView)fragmentView.findViewById(R.id.vm_fg_FragmentAnimation_tv_amnDauer);
        amnSpur = (TextView)fragmentView.findViewById(R.id.vm_fg_FragmentAnimation_tv_amnSpur);
    }

    private void initEvents() {
        fragmentView.setClickable(true);
        fragmentView.setOnClickListener(this);
        fragmentView.setOnLongClickListener(this);
    }
    private void updateViews(int spurID){
        if(spurID == -1) {
            Log.e("ERROR","SpurID fehler bei Übergabewert");
        }
        else {
            Log.i("ANIMATION", animation.getName());
            amnName.setText(animation.getName());
            amnDauer.setText("Dauer:" + animation.getAnimationDauer(spurID));
            amnSpur.setText("Spur:" + animation.getSpurName(spurID));
        }
    }


    public static VmFragmentAnimation erstelleSegment(String fileAnimation){
        Log.i("erzeugeFragment","static AmrFragmentSegment erstelleSegment()");
        VmFragmentAnimation fragment = new VmFragmentAnimation();
        Bundle b = new Bundle();
        b.putString(KEY_ANIMATION,fileAnimation);
        fragment.setArguments(b);
        return fragment;
    }

    @Override
    public void onClick(View view) {
        zeigePopUpWindow();
        Log.i("Stuff",animation.toString());

    }
    private void zeigePopUpWindow(){
        Log.i(getClass().getSimpleName(),"zeigePopUpWindow()");
        Intent intent = new Intent(getActivity(),VmSpurAuswahl.class);
        intent.putExtra(VmSpurAuswahl.KEY_SPUR,(Parcelable) animation);
        startActivityForResult(intent,REQUEST_CODE);
    }

    @Override
    public boolean onLongClick(View view) {
        erstelleDragView(view);
        entferneFragment(this);
        return true;
    }
    private void erstelleDragView(View view){
        ClipData data = ClipData.newPlainText("File", fileAnimation);
        View.DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(view);
        view.startDrag(data, shadowBuilder, view, 0);
    }

    @Override
    public void registriereBeobachter(Beobachter beobachter) {
        this.beobachter = beobachter;
    }

    @Override
     public void entferneFragment(Fragment fragment) {
         beobachter.aktualisieren(this, MeinFragmentEvent.FRAGMENT_ENTFERNEN);
     }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if(requestCode == REQUEST_CODE && resultCode == RESULT_OK) {
            int spurID = intent.getIntExtra(VmSpurAuswahl.KEY_SPUR_RESULT,-1);
            Log.i(getClass().getSimpleName(),"onActivityResult() -> SpurID = "+spurID);
            updateViews(spurID);
            aktiveSpur = spurID;
        }
    }

    public int getAktiveSpur(){
        return aktiveSpur;
    }
    public MyAnimation getAnimation(){
        return animation;
    }

}
