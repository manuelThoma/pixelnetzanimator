package ba_manuelthoma.pixelnetz.Memory;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import ba_manuelthoma.pixelnetz.Animation.MyAnimation;

/**
 * Created by thomama58961 on 06.12.2017.
 */

public class MemoryIntern implements IMemoryAmnLaden,IMemoryAmnSpeichern {



    //NICHT IMPLEMENTIERT
    @Override
    public void speichereAnimation(Context context, String fileName, MyAnimation animation) {
        try {
            FileOutputStream fos = context.openFileOutput(fileName+".amn", Context.MODE_PRIVATE);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(animation);
            oos.close();
            fos.close();
        }catch (IOException e){ Log.e("ERROR",e.getMessage());}
    }
    @Override
    public MyAnimation ladeAnimation(Context context,String fileName) {
        MyAnimation animation = new MyAnimation();
        try {
            FileInputStream fis = context.openFileInput(fileName);
            ObjectInputStream is = new ObjectInputStream(fis);
            animation = (MyAnimation) is.readObject();
            is.close();
            fis.close();
        }catch (IOException|ClassNotFoundException e){
            Log.e(getClass().getSimpleName(),e.getMessage());
        }

        return animation;
    }
}


