package ba_manuelthoma.pixelnetz.Memory;

import android.content.Context;
import android.graphics.Bitmap;

import ba_manuelthoma.pixelnetz.Animation.MyAnimation;

/**
 * Created by thomama58961 on 14.12.2017.
 */

public interface IMemoryAmnLaden {

    public MyAnimation ladeAnimation(Context context, String fileName);
}
