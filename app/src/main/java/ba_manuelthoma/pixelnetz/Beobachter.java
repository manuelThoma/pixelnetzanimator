package ba_manuelthoma.pixelnetz;


import ba_manuelthoma.pixelnetz.Animator.MeinFragmentEvent;

/**
 * Created by thomama58961 on 13.11.2017.
 */

public interface Beobachter {
    public void aktualisieren(BeobachtbaresSubjekt b, MeinFragmentEvent event);
}
