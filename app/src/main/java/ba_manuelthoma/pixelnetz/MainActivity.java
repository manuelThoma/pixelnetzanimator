package ba_manuelthoma.pixelnetz;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.wifi.WifiManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;


import com.example.student.pixelnetz.Bibliothek.verbindung.Netzwerk;

import ba_manuelthoma.pixelnetz.Animator.Animator;
import ba_manuelthoma.pixelnetz.VideoManager.VideoManager;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    private final String NETZWERKNAME = "DeathNoteSpot";
    private final String NETZPASSWORT = "sehtmichan";

    public final int MY_PERMISSIONS_REQUEST_WRITE_STORAGE = 134;

    private Button animator;
    private Button videomanager;

    private Netzwerk netzwerk;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        holeViews();
        initEvents();
        initNetzwerk();
        initCustonExceptionHandler();
    }
    private void holeViews() {
        animator = (Button)findViewById(R.id.act_MainActivity_btn_animator);
        videomanager = (Button)findViewById(R.id.act_MainActivity_btn_videomanager);
    }
    private void initEvents() {
        animator.setOnClickListener(this);
        videomanager.setOnClickListener(this);
    }
    private void initNetzwerk(){
        WifiManager wifiManager = (WifiManager) this.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        netzwerk = new Netzwerk(wifiManager);
        netzwerk.verbinden(NETZWERKNAME,NETZPASSWORT);
        while(netzwerk.isVerbindungAktiv());
    }

    @Override
    public void onClick(View view) {
        if(view == animator){
            Log.i(getLocalClassName(),"Starte Activity Animator");
            Intent intent = new Intent(MainActivity.this, Animator.class);
            startActivity(intent);
        }if(view == videomanager) {
            Log.i(getLocalClassName(), "Starte Activity VideoManager");
            Intent intent = new Intent(MainActivity.this, VideoManager.class);
            startActivity(intent);
        }
    }
    private void initCustonExceptionHandler(){
        holeErlaubnisAufSchreibZugriff();
        Thread.setDefaultUncaughtExceptionHandler(new CustomizedExceptionHandler(
                "/mnt/sdcard/"));
    }

    private void holeErlaubnisAufSchreibZugriff(){

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        MY_PERMISSIONS_REQUEST_WRITE_STORAGE);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
            Log.i("holeErlaubnisAufZugriff","Zugriff verweigert");
            //return false;
        }
        Log.i("holeErlaubnisAufZugriff","Zugriff genehmigt");
        //return true;
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[],
                                           int[] grantResults) {
        Log.i(getClass().getSimpleName(), "onRequestPermissionsResult()");
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_WRITE_STORAGE:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.i(getLocalClassName(), "onRequestPermissionsResult GRANTED, NOW SCREW AROUND");
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {
                    Log.i(getLocalClassName(), "onRequestPermissionsResult DENIEEEED");

                    //exit application
                    this.finishAffinity();
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            // other 'case' lines to check for other
            // permissions this app might request
        }
    }
}
