package ba_manuelthoma.pixelnetz.Animator;

import android.app.Fragment;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Toast;


import java.util.ArrayList;
import java.util.List;

import ba_manuelthoma.pixelnetz.Animation.MyAnimation;
import ba_manuelthoma.pixelnetz.BeobachtbaresSubjekt;
import ba_manuelthoma.pixelnetz.Beobachter;
import ba_manuelthoma.pixelnetz.Memory.IMemoryAmnSpeichern;
import ba_manuelthoma.pixelnetz.Memory.MemoryIntern;
import ba_manuelthoma.pixelnetz.R;
import ba_manuelthoma.pixelnetz.EnterNameField;
import ba_manuelthoma.pixelnetz.VideoManager.VideoManager;

/**
 * Created by thomama58961 on 10.11.2017.
 */

public class Animator extends AppCompatActivity implements Beobachter,View.OnClickListener{

    public final int MY_PERMISSIONS_REQUEST_READ_PICTURES = 134;
    private final static int REQUEST_CODE_AMN_NAME = 1875;

    //View
    private ImageButton btnAnimationSpeichern;
    private ImageButton btnAnimationVerwerfen;
    private ImageButton btnVideoManager;
    private LinearLayout llySpuren;
    private Button btnAddSpur;

    private AmrGalerie fgAmrGalerie;
    private List<AmrFragmentSpur> list_spuren = new ArrayList<>();

    //Daten
    private MyAnimation animation;
    private String animationName = "keinName";

    //Memory
    IMemoryAmnSpeichern memory = new MemoryIntern();


    @Override
    protected void onCreate(Bundle b) {
        super.onCreate(b);
        setContentView(R.layout.activity_animator);
        holeViews();
        initEvents();
    }
    private void holeViews(){
        btnAnimationSpeichern = (ImageButton)findViewById(R.id.amr_act_Animator_btn_animationSpeichern);
        btnAnimationVerwerfen = (ImageButton)findViewById(R.id.amr_act_Animator_btn_animationVerwerfen);
        btnVideoManager = (ImageButton)findViewById(R.id.amr_act_Animator_btn_videomanager);
        btnAddSpur = (Button)findViewById(R.id.amr_act_Animator_btn_spurHinzufügen);
        llySpuren = (LinearLayout)findViewById(R.id.amr_act_Animator_lly_spurListe);
    }
    private void initEvents() {
        btnAddSpur.setOnClickListener(this);
        btnAnimationSpeichern.setOnClickListener(this);
        btnAnimationVerwerfen.setOnClickListener(this);
        btnVideoManager.setOnClickListener(this);
    }
    @Override
    public void onClick(View view) {
        if(view == btnAddSpur){
            erstelleNeueSpur();
        }if(view==btnAnimationSpeichern) {
            animationSpeichern();
        }if(view== btnAnimationVerwerfen){
            animationVerwerfen();
        }if(view==btnVideoManager){
            starteVideoManager();
        }
    }

    /**
     * Methode welche ein neues Fragment AmrFragmentSpur erstellt
     * dieses in liste_spuren ablegt und sich bei dem
     * Fragment AmrFragmentSpur als Beobachter einträgt
     */
    private void erstelleNeueSpur(){
        AmrFragmentSpur amrFragmentSpur = AmrFragmentSpur.erstelleSpur();
        getFragmentManager().beginTransaction().add(llySpuren.getId(),
                amrFragmentSpur, "tag").commit();
        list_spuren.add(amrFragmentSpur);
        amrFragmentSpur.registriereBeobachter(this);

    }

    private void animationSpeichern(){
        animation = holeAnimationZumSpeichern();
        Intent intent = new Intent(this, EnterNameField.class);
        startActivityForResult(intent,REQUEST_CODE_AMN_NAME);
    }
    private void animationInSpeicherSchreiben(){
        Toast.makeText(this, "Animation gespeichert", Toast.LENGTH_SHORT).show();
        memory.speichereAnimation(this,animationName,animation);
        Log.i(getClass().getSimpleName(),"Animation wird gespeichert");
    }

    private void animationVerwerfen(){

        Toast.makeText(this, "Animation verworfen", Toast.LENGTH_SHORT).show();
        for(AmrFragmentSpur fragmentSpur: list_spuren) {
            getFragmentManager().beginTransaction().remove((Fragment) fragmentSpur).commit();
        }
        list_spuren.clear();
    }
    private void starteVideoManager(){
        Intent intent = new Intent(this, VideoManager.class);
        startActivity(intent);
    }

    @Override
    public void aktualisieren(BeobachtbaresSubjekt b, MeinFragmentEvent event) {
        getFragmentManager().beginTransaction().remove((Fragment) b).commit();
        list_spuren.remove(b);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[],
                                           int[] grantResults) {
        Log.i(getClass().getSimpleName(), "onRequestPermissionsResult()");
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_PICTURES:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.i(getLocalClassName(), "onRequestPermissionsResult GRANTED, NOW SCREW AROUND");
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {
                    Log.i(getLocalClassName(), "onRequestPermissionsResult DENIEEEED");
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    public MyAnimation holeAnimation(){
        Log.i(getClass().getSimpleName(), "holeAnimation()");
        animation = null;
        animation = new MyAnimation();
        int i = 0;

        for (AmrFragmentSpur spur: list_spuren) {
            animation.addSpuren(spur.holeSpur());
        }
        return animation;
    }

    public MyAnimation holeAnimationZumSpeichern(){
        Log.i(getClass().getSimpleName(), "holeAnimationZumSpeichern()");
        animation = null;
        animation = new MyAnimation();

        for (AmrFragmentSpur spur: list_spuren) {
            animation.addSpuren(spur.holeSpurZumSpeichern());
        }
        return animation;
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if(requestCode == REQUEST_CODE_AMN_NAME && resultCode == RESULT_OK) {
            animationName = intent.getStringExtra(EnterNameField.KEY_NAME_RESULT);
            animation.setName(animationName);
            animationInSpeicherSchreiben();
        }

    }
    @Override
    public void onResume(){
        super.onResume();
        for (AmrFragmentSpur fragmentSpur: list_spuren) {
            fragmentSpur.registriereBeobachter(this);
        }
    }
}