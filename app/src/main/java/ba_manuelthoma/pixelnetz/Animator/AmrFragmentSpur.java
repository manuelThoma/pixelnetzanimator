package ba_manuelthoma.pixelnetz.Animator;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.DragEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ToggleButton;



import java.util.ArrayList;
import java.util.List;

import ba_manuelthoma.pixelnetz.Animation.MyAnimationSpur;
import ba_manuelthoma.pixelnetz.BeobachtbaresSubjekt;
import ba_manuelthoma.pixelnetz.Beobachter;
import ba_manuelthoma.pixelnetz.EnterNameField;
import ba_manuelthoma.pixelnetz.R;

/**
 * Created by thomama58961 on 10.11.2017.
 * OnTouchListener in die Views
 */
//TODO implement InterfaceAnzeige
public class AmrFragmentSpur extends Fragment implements Beobachter,BeobachtbaresSubjekt,View.OnClickListener, View.OnDragListener{

    private final static String KEY_ID = "KEY_ID";

    private final static int REQUEST_CODE_SPUR_NAME = 1821;

    private static int helpCounter = 1;

    //Views
    private ImageButton btnRemove;
    private ToggleButton tbtnSpurAnzeigen;
    private View fragmentView;
    private TextView tvName;
    private LinearLayout llySpur;

    //Data
    private List<AmrFragmentSegment> list_fg_segmente = new ArrayList<>();
    private MyAnimationSpur anzeigen = new MyAnimationSpur();
    private String spurName = "";


    private Beobachter beobachter;

    @Override
    public void onCreate(Bundle b)
    {
        Log.i(getClass().getSimpleName(),"onCreate()");
        super.onCreate(b);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle bundle)
    {
        Log.i(getClass().getSimpleName(),"onCreateView()");
        fragmentView = (ViewGroup) inflater.inflate(R.layout.fragment_spur,container,false);
        holeViews();
        initEvents();
        enterName();
        return fragmentView;
    }

    private void holeViews() {
        btnRemove = (ImageButton) fragmentView.findViewById(R.id.amr_fg_Spur_btn_spurEntfernen);
        tbtnSpurAnzeigen = (ToggleButton) fragmentView.findViewById(R.id.amr_fg_Spur_tbtn_Einklappen);
        llySpur = (LinearLayout)fragmentView.findViewById(R.id.amr_fg_Spur_lly_segmentListe);
        tvName = (TextView)fragmentView.findViewById(R.id.amr_fg_Spur_tv_spurName);
    }
    private void initEvents()
    {
        btnRemove.setOnClickListener(this);
        tbtnSpurAnzeigen.setOnClickListener(this);
        llySpur.setOnDragListener(this);
        tvName.setClickable(true);
        tvName.setOnClickListener(this);
    }
    private void enterName(){
        Intent intent = new Intent(getActivity().getApplicationContext(), EnterNameField.class);
        startActivityForResult(intent,REQUEST_CODE_SPUR_NAME);
    }
    private void setSpurName(){
        tvName.setText(spurName);
    }

    public static AmrFragmentSpur erstelleSpur(){
        Log.i("erzeugeFragment","static AmrFragmentSpur erstelleSpur()");
        AmrFragmentSpur fragment = new AmrFragmentSpur();
        Bundle b = new Bundle();
        fragment.setArguments(b);
        return fragment;
    }

    @Override
    public void entferneFragment(Fragment fragment)
    {
        Log.i(getClass().getSimpleName(),"entferneFragment()");
        beobachter.aktualisieren(this,MeinFragmentEvent.FRAGMENT_ENTFERNEN);
        onDestroy();
    }

    //Beaobachter
    public void registriereBeobachter(Beobachter beobachter)
    {
        Log.i(getClass().getSimpleName(),"registriereBeobachter()");
        this.beobachter = beobachter;
    }

    @Override
    public void onClick(View view) {
        if(view == btnRemove){
            entferneFragment(this);
        }if(view == tbtnSpurAnzeigen) {
            if(tbtnSpurAnzeigen.isChecked()){
                llySpur.setVisibility(View.VISIBLE);
            }else{
                llySpur.setVisibility(View.GONE);
            }
        }if(view == tvName){
            enterName();

        }
    }

    @Override
    public boolean onDrag(View view, DragEvent event){
        int action = event.getAction();
        switch (action) {
            case DragEvent.ACTION_DRAG_STARTED:
                return true;
            case DragEvent.ACTION_DRAG_LOCATION:
                return true;
            case DragEvent.ACTION_DROP:
                Log.i("ON_DROP",event.getClipData().getItemAt(0).getText().toString());
                String bitmappath = event.getClipData().getItemAt(0).getText().toString();
                int pos = setzeMeineIndexPos(view,event);
                erstelleNeuesSegment(bitmappath,pos);
                return true;
            case DragEvent.ACTION_DRAG_ENDED:
                return true;
        }
        return false;
    }

    /**
     * Methode welche die Position der View in einem Layout festlegt
     *
     * @param layout ist der Container in dem die View plaziert werden soll
     * @param element ist die View welche im Container plaziert wird
     * @return die IndexNummer des Layouts welche die View einnehmen soll
     */
    private int setzeMeineIndexPos(View layout,DragEvent element) {
        int letztePos = ((ViewGroup)layout).getChildCount();
        for(int index=0; index<((ViewGroup)layout).getChildCount(); ++index) {
            View nextChild = ((ViewGroup)layout).getChildAt(index);
            if(element.getX() < (nextChild.getX()+nextChild.getWidth()/2)) {
                return index;
            }
        }
        return letztePos;
    }

    private void erstelleNeuesSegment(String bmppath,int position) {

        LinearLayout container = new LinearLayout(getActivity().getApplicationContext());
        container.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT));
        //TODO containerID dynamisch setzen
        container.setId(1000+helpCounter++);

        AmrFragmentSegment segmentFm = AmrFragmentSegment.erstelleSegment(bmppath);
        getFragmentManager().beginTransaction().add(container.getId(),
                segmentFm, "tag").commit();
        list_fg_segmente.add(segmentFm);
        segmentFm.registriereBeobachter(this);
        llySpur.addView(container, position);
    }

    @Override
    public void aktualisieren(BeobachtbaresSubjekt b, MeinFragmentEvent event) {
        if(event == MeinFragmentEvent.FRAGMENT_ENTFERNEN) {
            getFragmentManager().beginTransaction().remove((Fragment) b).commit();
            list_fg_segmente.remove(b);
        }
    }

    public MyAnimationSpur holeSpur(){
        setSegmentPos();
        for(AmrFragmentSegment segmentFm:list_fg_segmente){
            anzeigen.addSegments(segmentFm.holeSegment());
        }
        anzeigen.setName(spurName);
        return anzeigen;
    }

    public MyAnimationSpur holeSpurZumSpeichern(){
        setSegmentPos();
        for(AmrFragmentSegment segmentFm:list_fg_segmente){
            anzeigen.addSegments(segmentFm.holeSegmentZumSpeichern());
        }
        anzeigen.setName(spurName);
        return anzeigen;
    }

    private void setSegmentPos(){
        for(int index = 0;index<llySpur.getChildCount();index++) {
            int fgID = llySpur.getChildAt(index).getId();
            for(AmrFragmentSegment fragment:list_fg_segmente)
            {
                if(fgID == fragment.getId()){
                    fragment.setzeAnzeigePos(index);
                    break;
                }
            }
        }
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if(requestCode == REQUEST_CODE_SPUR_NAME && resultCode == Activity.RESULT_OK) {
            spurName = intent.getStringExtra(EnterNameField.KEY_NAME_RESULT);
            setSpurName();
        }
    }



    @Override
    public void onDestroy(){
        for(AmrFragmentSegment segment:list_fg_segmente){
            segment.onDestroy();
        }
        Log.i(getClass().getSimpleName(),"onDestroy()");
        super.onDestroy();
    }
}