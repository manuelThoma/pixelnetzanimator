package ba_manuelthoma.pixelnetz.Animator;

import android.content.ClipData;
import android.content.Context;
import android.net.Uri;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import ba_manuelthoma.pixelnetz.R;

/**
 * Created by thomama58961 on 30.11.2017.
 */

public class ImageAdapter extends BaseAdapter {
    private Context mContext;
    List<String> itemList = new ArrayList<>();

    public ImageAdapter(Context c){
        mContext = c;
    }
    public void add(String path) {
        //Log.i("Add Image to List",path);
        //Log.i("Give Image Pos:",""+itemList.size());
        itemList.add(path);
    }

    public void ItemListZuruecksetzen(){itemList.clear();}

    @Override
    public int getCount() {
        return itemList.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    public String getBitmappath(int id){return itemList.get(id);}

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ImageView imageView;

        if(convertView == null){
            imageView = new ImageView(mContext);
            GridView.LayoutParams params = new GridView.LayoutParams(
                    (int)mContext.getResources().getDimension(R.dimen.fg_bildersammlung_bild_weite),
                    (int)mContext.getResources().getDimension(R.dimen.fg_bildersammlung_bild_hoehe));
            imageView.setLayoutParams(params);
            imageView.setClickable(true);
            imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
            imageView.setPadding(2,2,2,2);

        }else{
        imageView = (ImageView) convertView;
        }
        imageView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                //Log.i("Adapter-BitmapPath:",itemList.get(position));
                //Log.i("Adapter-ItemPosition",""+position);
                ClipData data = ClipData.newPlainText("BitmapPath",itemList.get(position));
                View.DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(view);
                view.startDrag(data,shadowBuilder,view,0);
                return true;
            }
        });
        Glide.with(mContext).load(itemList.get(position)).into(imageView);
        return imageView;
    }
}
