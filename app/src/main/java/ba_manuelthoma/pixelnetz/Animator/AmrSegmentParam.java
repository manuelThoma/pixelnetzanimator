package ba_manuelthoma.pixelnetz.Animator;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import ba_manuelthoma.pixelnetz.R;

/**
 * Created by thomama58961 on 04.12.2017.
 */

public class AmrSegmentParam extends AppCompatActivity implements SeekBar.OnSeekBarChangeListener{

    public static final String KEY_DAUER = "KEY_DAUER";
    public static final String KEY_DAUER_RESULT = "KEY_DAUER_RESULT";
    public static final String KEY_BILD = "KEY_BILD";
    private final float skbarScaling = 10; //auf eine Nachkommastelle da Seekbar nur int werde kann
    private final float skbarMaxScaling = 1.2f; //Anpassung des Max Wertes
    private final int skbarOffset = 5;

    private float dauer;
    private TextView tvDauer;
    private SeekBar skbarDauer;
    private ImageView ivSegment;

    @Override
    protected void onCreate(Bundle b) {
        Log.i(getClass().getSimpleName(),"onCreate()");
        super.onCreate(b);
        setContentView(R.layout.popup_activity_amr_segement);
        holeViews();
        Intent intent = getIntent();
        dauer = intent.getFloatExtra(KEY_DAUER, 1);
        tvDauer.setText("" + dauer);
        String bmppath = intent.getStringExtra(KEY_BILD);
        Glide.with(this).load(bmppath).into(ivSegment);
        initPopUp();
        initSeekbar();
        initEvents();
    }

    private void initSeekbar() {
        skbarDauer.setMax((int) (dauer * skbarScaling * skbarMaxScaling) + skbarOffset);
        skbarDauer.setProgress((int) (dauer * skbarScaling));
    }

    private void initPopUp() {
        Log.i(getClass().getSimpleName(),"initPopUp()");
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        getWindow().setGravity(Gravity.TOP);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        int width = dm.widthPixels;
        int height = dm.heightPixels;
        getWindow().setLayout((int) (width * .6), (int) (height * .6));
    }

    private void holeViews() {
        ivSegment = (ImageView) findViewById(R.id.amr_popUp_SegmentParam_ImageView);
        skbarDauer = (SeekBar) findViewById(R.id.amr_popUp_SegmentParam_Seekbar);
        tvDauer = (TextView) findViewById(R.id.amr_popUp_SegmentParam_TextView);
    }

    private void initEvents() {
        skbarDauer.setOnSeekBarChangeListener(this);
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean b) {
        dauer = (float) progress / skbarScaling;
        tvDauer.setText("" + dauer);
        seekBar.setMax((int) (seekBar.getProgress() * skbarMaxScaling) + skbarOffset);
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        Log.i(getLocalClassName(),"onStopTracking");
        Intent intent = new Intent();
        intent.putExtra(KEY_DAUER_RESULT, dauer);
        setResult(Activity.RESULT_OK, intent);
    }
    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {/*Empty*/}
}
