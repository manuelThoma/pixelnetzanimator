package ba_manuelthoma.pixelnetz.Animator;

import android.Manifest;
import android.app.Fragment;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.Spinner;


import java.io.File;
import java.io.IOException;
import java.util.Scanner;

import ba_manuelthoma.pixelnetz.R;

import static android.os.Environment.getExternalStoragePublicDirectory;

/**
 * Created by thomama58961 on 10.11.2017.
 */

public class AmrGalerie extends Fragment implements AdapterView.OnItemSelectedListener{

    private View fragmentView;
    private GridView gridView;
    private ImageAdapter mImageAdapter;
    private Spinner spinMenu;

    public final int MY_PERMISSIONS_REQUEST_READ_PICTURES = 134;

    @Override
    public void onCreate(Bundle b)
    {
        super.onCreate(b);
        mImageAdapter = new ImageAdapter(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle bundle)
    {
        fragmentView = inflater.inflate(R.layout.fragment_bildersammlung,container,false);
        holeViews();
        initEvents();
        return fragmentView;
    }

    private void holeViews() {
        gridView = (GridView)fragmentView.findViewById(R.id.amr_fg_Bildersammlung_gv_bilder);

        spinMenu = (Spinner)fragmentView.findViewById(R.id.amr_fg_Bildersammlung_spin_laden);
        ArrayAdapter<CharSequence>adapter = ArrayAdapter.createFromResource(
                getActivity().getApplicationContext(),R.array.laden_auswahlmenu,
                android.R.layout.simple_spinner_dropdown_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinMenu.setAdapter(adapter);
    }
    private void initEvents(){

        spinMenu.setOnItemSelectedListener(this);
    }

    public GridView getGridView() {
        return gridView;
    }

    private void ladeBilderAusCamera(){
        //TODO Auslagern in MemoryExtern
        gridView.setAdapter(mImageAdapter);
        File directory = getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM +File.separator+"Camera");
        if(directory.exists()) {
            File[] files = directory.listFiles();
            for (File file : files) {
                if(!file.isDirectory()) {
                    mImageAdapter.add(file.getAbsolutePath());
                }
            }
        }
    }
    private boolean ladeBilderAusAsset(String rootfile){
        //TODO Auslagern in Asset
        String [] list;
        gridView.setAdapter(mImageAdapter);
        try {
            list = getActivity().getAssets().list(rootfile);
            if (list.length > 0) {

                // This is a folder
                for (String file : list) {
                    if (!ladeBilderAusAsset(rootfile + "/" + file))
                        return false;
                    else {
                        //Log.i("List","\"file:///android_asset/\"+rootfile + \"/\" + file");
                        mImageAdapter.add("file:///android_asset/"+rootfile + "/" + file);
                        // This is a file
                    }
                }
            }
        } catch (IOException e) {
                return false;
        }

        return true;
    }

    private void holeErlaubnisAufZugriff(){

        if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                    Manifest.permission.READ_EXTERNAL_STORAGE)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        MY_PERMISSIONS_REQUEST_READ_PICTURES);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
            Log.i("holeErlaubnisAufZugriff","Zugriff verweigert");
            //return false;
        }
        Log.i("holeErlaubnisAufZugriff","Zugriff genehmigt");
        //return true;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String selected = parent.getItemAtPosition(position).toString();
        Scanner scanner = new Scanner(selected).useDelimiter("\\s*:\\s");
        if(scanner.hasNext()){
            String next = scanner.next();
            if(next.equals("Asset")&&scanner.hasNext()){
                try {
                    String [] list;
                    list = getActivity().getAssets().list("");
                    for (String file : list)
                    mImageAdapter.ItemListZuruecksetzen();
                    ladeBilderAusAsset(scanner.next());
                }catch (Exception e){ Log.e("LadeFehler",e.getMessage().toString());}
            }
            if(next.equals("Extern")){
                try {
                    mImageAdapter.ItemListZuruecksetzen();
                    holeErlaubnisAufZugriff();
                    ladeBilderAusCamera();
                }catch (Exception e){ Log.e("LadeFehler",e.getMessage().toString());}
            }
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
