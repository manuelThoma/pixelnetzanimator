package ba_manuelthoma.pixelnetz.Animator;

/**
 * Created by thomama58961 on 13.11.2017.
 */

public enum MeinFragmentEvent
{
    FRAGMENT_ENTFERNEN,
    FRAGMENT_POSITION,
    FRAGMENT_AKTUALISIEREN;
}
