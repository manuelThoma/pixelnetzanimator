package ba_manuelthoma.pixelnetz.Animator;

import android.app.Activity;
import android.app.Fragment;
import android.content.ClipData;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import ba_manuelthoma.pixelnetz.Animation.MyAnimationSegment;
import ba_manuelthoma.pixelnetz.Animation.SegmentBildpfad;
import ba_manuelthoma.pixelnetz.Animation.SegmentBitmap;
import ba_manuelthoma.pixelnetz.BeobachtbaresSubjekt;
import ba_manuelthoma.pixelnetz.Beobachter;
import ba_manuelthoma.pixelnetz.R;


/**
 * Created by thomama58961 on 20.11.2017.
 */

public class AmrFragmentSegment extends Fragment implements BeobachtbaresSubjekt,View.OnClickListener, View.OnLongClickListener{

    public static final String IMAGE_KEY = "IMAGE_KEY";
    private final float DEFAULT_DAUER = 1.0f;

    private final static int REQUEST_CODE = 4875;

    //Views
    private View fragmentView;
    private LinearLayout lly_AnimationSegment_ImageView;
    private ImageView imageView;
    private TextView tvDauer;

    //Daten
    MyAnimationSegment segment;
    private int anzeigePos;
    private String bitmappath;
    private float dauer = DEFAULT_DAUER;

    //Anzeige die dieses Segment enthält
    private Beobachter beobachter;

    /***********************************************************************************************
     *                                Initialisierung                                              *
     **********************************************************************************************/
    @Override
    public void onCreate(Bundle b)
    {
        Log.i(getClass().getSimpleName(),"onCreate()");
        super.onCreate(b);
        Bundle args = getArguments();
        bitmappath = args.getString(IMAGE_KEY);
        if(args != null)
        {
            try {
                initImageView();
            }catch (Exception e){
                Log.e(getClass().getSimpleName(),e.getMessage());
            }
        }
    }
    private void initImageView() throws Exception{
        Log.i(getClass().getSimpleName(),"initImageView()");
        imageView = new ImageView(getActivity().getApplicationContext());
        imageView.setClickable(true);
        imageView.setOnClickListener(this);
        imageView.setOnLongClickListener(this);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                (int)getResources().getDimension(R.dimen.fg_animation_segment_bild_weite),
                (int)getResources().getDimension(R.dimen.fg_animation_segment_bild_hoehe));
        params.gravity = Gravity.CENTER;
        imageView.setLayoutParams(params);
        Glide.with(this).load(bitmappath).into(imageView);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle bundle) {
        Log.i(getClass().getSimpleName(),"onCreateView()");
        fragmentView = inflater.inflate(R.layout.fragment_animations_segment, container,false);
        holeViews();
        return fragmentView;
    }
    private void holeViews(){
        lly_AnimationSegment_ImageView = (LinearLayout)
                fragmentView.findViewById(R.id.amr_fg_AnimationsSegment_lly_segment);
        lly_AnimationSegment_ImageView.addView(imageView);

        //TextView
        tvDauer =(TextView)fragmentView.findViewById(R.id.amr_fg_AnimationsSegment_tv_dauer);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,ViewGroup.LayoutParams.WRAP_CONTENT);
        params.gravity = Gravity.CENTER;
        tvDauer.setLayoutParams(params);
        tvDauer.setText(dauer + " sek");
    }
    public static AmrFragmentSegment erstelleSegment(String bmppath){
        Log.i("erzeugeFragment","static AmrFragmentSegment erstelleSegment()");
        AmrFragmentSegment fragment = new AmrFragmentSegment();
        Bundle b = new Bundle();
        b.putString(IMAGE_KEY, bmppath);
        fragment.setArguments(b);
        return fragment;
    }
    @Override
    public void registriereBeobachter(Beobachter beobachter) {
        this.beobachter=beobachter;
    }

    /***********************************************************************************************
     *                                Listener                                                     *
     **********************************************************************************************/
    @Override
    public void onClick(View view) {
        zeigePopUpWindow();
    }
    private void zeigePopUpWindow(){
        Log.i(getClass().getSimpleName(),"zeigePopUpWindow()");
        Intent intent = new Intent(getActivity(),AmrSegmentParam.class);
        intent.putExtra(AmrSegmentParam.KEY_BILD,bitmappath);
        intent.putExtra(AmrSegmentParam.KEY_DAUER,dauer);
        startActivityForResult(intent,REQUEST_CODE);
    }

    @Override
    public boolean onLongClick(View view) {
        erstelleDragView(view);
        entferneFragment(this);
        return true;
    }
    private void erstelleDragView(View view){
        ClipData data = ClipData.newPlainText("BitmapPath", bitmappath);
        View.DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(view);
        view.startDrag(data, shadowBuilder, view, 0);
    }

    /***********************************************************************************************
     *                                Getter/Setter                                                *
     **********************************************************************************************/
    public Bitmap holeAnimationSegmentBitmap(){
        Bitmap bitmap = ((BitmapDrawable)imageView.getDrawable()).getBitmap();
        return bitmap;
    }
    public float holeAnimationSegmentDauerInSek(){
        return dauer;
    }

    public MyAnimationSegment holeSegment(){
        segment = new SegmentBildpfad(
               holeAnimationSegmentDauerInSek(),anzeigePos, bitmappath);
        return segment;
    }
    public MyAnimationSegment holeSegmentZumSpeichern(){
        segment = new SegmentBitmap(
                holeAnimationSegmentDauerInSek(),anzeigePos, holeAnimationSegmentBitmap());
        return segment;
    }
    public void setzeAnzeigePos(int pos){
        anzeigePos = pos;
    }


    /***********************************************************************************************
     *                                ???                                                          *
     **********************************************************************************************/
    @Override
    public void entferneFragment(Fragment fragment) {
        beobachter.aktualisieren(this,MeinFragmentEvent.FRAGMENT_ENTFERNEN);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if(requestCode == REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            dauer = intent.getFloatExtra(AmrSegmentParam.KEY_DAUER_RESULT,0.0f);
            Log.i(getClass().getSimpleName(),"onActivityResult() -> dauer = "+dauer);
            tvDauer.setText(dauer+"sek");
        }
    }

    @Override
    public void onDestroy(){
        Log.i(getClass().getSimpleName(),"onDestory()");
        super.onDestroy();
    }
}
