package ba_manuelthoma.pixelnetz.Animation;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by thomama58961 on 27.11.2017.
 */

public class MyAnimationSpur implements Parcelable,Serializable {

    private List<MyAnimationSegment> segments= new ArrayList<>();
    private String name = "Empty";

    public MyAnimationSpur(){/*Empty*/}

    public MyAnimationSpur(Parcel in) {
        segments = new ArrayList<MyAnimationSegment>();
        in.readList(segments,MyAnimationSegment.class.getClassLoader());
        this.name = in.readString();
    }

    public static final Creator<MyAnimationSpur> CREATOR = new Creator<MyAnimationSpur>() {
        @Override
        public MyAnimationSpur createFromParcel(Parcel in) {
            return new MyAnimationSpur(in);
        }

        @Override
        public MyAnimationSpur[] newArray(int size) {
            return new MyAnimationSpur[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(segments);
        dest.writeString(name);
    }

    public void addSegments(MyAnimationSegment seg){
        segments.add(seg);
    }
    public List<MyAnimationSegment> getSegments(){
        return segments;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
        int i = 0;
        sb.append("Animation->AnzeigenList["+ segments.size()+"]\n");
        for(MyAnimationSegment a: segments) {
            sb.append("\tAnzeigenList["+(i++)+"]\n");
            sb.append("\t"+a.toString());
        }
        return sb.toString();
    }

    public float getSpurDauer(){
        float spurDauer = 0;
        for (MyAnimationSegment seg : segments) {
            spurDauer += seg.getDauer();
        }
        return spurDauer;
    }
}
