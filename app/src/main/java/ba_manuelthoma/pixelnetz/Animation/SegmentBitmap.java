package ba_manuelthoma.pixelnetz.Animation;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Parcel;
import android.os.Parcelable;

import java.io.ByteArrayOutputStream;

/**
 * Created by thomama58961 on 29.11.2017.
 */

public class SegmentBitmap extends MyAnimationSegment {

    private byte[] imageByteArray;
    //private Bitmap bitmap;

    public Bitmap getBitmap() {
        return decodeBitmap();
    }
    public void setBitmap(Bitmap bitmap) {
        compressBitmap(bitmap);
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public SegmentBitmap createFromParcel(Parcel in) {
            return new SegmentBitmap(in);
        }
        public SegmentBitmap[] newArray(int size) {
            return new SegmentBitmap[size];
        }
    };


    public SegmentBitmap(float dauer,int position,Bitmap bitmap){//String bitmappath
        super(dauer,position);
        compressBitmap(bitmap);
    }

    public SegmentBitmap(Parcel in){
        super(in);
        imageByteArray = new byte[in.readInt()];
        in.readByteArray(imageByteArray);
    }
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest,flags);
        dest.writeInt(imageByteArray.length);
        dest.writeByteArray(imageByteArray);

    }

    private void compressBitmap(Bitmap bitmap){
        ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG,0,byteStream);
        imageByteArray = byteStream.toByteArray();
    }
    private Bitmap decodeBitmap(){
        Bitmap bmp;
        bmp = BitmapFactory.decodeByteArray(imageByteArray,0,imageByteArray.length);
        return bmp;
    }

    @Override
    public String toString(){
        String string = super.toString();
        StringBuilder sb = new StringBuilder();
        sb.append(string);
        sb.append("\t\t\t\t\t\t\tBitmap: "+decodeBitmap().toString()+"\n");
        return sb.toString();
    }
}
