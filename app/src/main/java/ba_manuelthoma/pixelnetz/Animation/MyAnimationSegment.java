package ba_manuelthoma.pixelnetz.Animation;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

/**
 * Created by thomama58961 on 27.11.2017.
 */

public abstract class MyAnimationSegment implements Parcelable,Serializable {

    private float dauer;
    private int position;

    
    // Constructor
    public MyAnimationSegment(){/*Empty*/}

    public MyAnimationSegment(float dauer){
        this.dauer = dauer;
    }
    public MyAnimationSegment(float dauer,int position){
        this.dauer = dauer;
        this.position = position;
    }


    // Parcelling part
    public MyAnimationSegment(Parcel in){
        this.dauer = in.readFloat();
        this.position = in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeFloat(this.dauer);
        dest.writeInt(this.position);
    }

    public float getDauer() {
        return dauer;
    }
    public void setDauer(float dauer) {
        this.dauer = dauer;
    }


    public int getPosition() {
        return position;
    }
    public void setPosition(int position) {
        this.position = position;
    }

    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append("\tSegment->\n");
        sb.append("\t\t\t\t\t\t\tDauer: "+dauer+"\n");;
        sb.append("\t\t\t\t\t\t\tAnzeigePos: "+ position +"\n");
        return sb.toString();
    }

}
