package ba_manuelthoma.pixelnetz.Animation;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by thomama58961 on 27.11.2017.
 */

public class MyAnimation implements Parcelable,Serializable{

    private List<MyAnimationSpur> spuren = new ArrayList<>();
    private long startZeitpunkt = 0;
    private String name = "keinName";

    public MyAnimation(){/*Empty*/}

    public MyAnimation(Parcel in) {
        spuren = new ArrayList<MyAnimationSpur>();
        in.readTypedList(spuren, MyAnimationSpur.CREATOR);
        name = in.readString();

    }

    public static final Creator<MyAnimation> CREATOR = new Creator<MyAnimation>() {
        @Override
        public MyAnimation createFromParcel(Parcel in) {
            return new MyAnimation(in);
        }

        @Override
        public MyAnimation[] newArray(int size) {
            return new MyAnimation[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(spuren);
        dest.writeString(name);
    }

    public void addSpuren(MyAnimationSpur element){
        spuren.add(element);
    }
    public List<MyAnimationSpur> getSpuren(){
        return spuren;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public long getStartZeitpunkt() {
        return startZeitpunkt;
    }
    public void setStartZeitpunkt(long startZeitpunkt) {
        this.startZeitpunkt = startZeitpunkt;
    }


    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
        int i = 0;
        sb.append("Animation->AnzeigenList["+ spuren.size()+"]\n");
        for(MyAnimationSpur a: spuren) {
            sb.append("\tAnzeigenList["+(i++)+"]\n");
            sb.append("\t"+a.toString());
        }
        return sb.toString();
    }

    public float getAnimationDauer(int spur){

        float spurDauer = 0;
        if(spur >= 0 && spur < spuren.size()) {
            spurDauer = spuren.get(spur).getSpurDauer();
        }else
            Log.e(getClass().getSimpleName(),"Spur exsistiert nicht");
        return spurDauer;
    }
    public String getSpurName(int spur){
        String string = "";
        if(spur >= 0 && spur < spuren.size()) {
            string = spuren.get(spur).getName();
        }else
            Log.e(getClass().getSimpleName(),"Spur exsistiert nicht");
        return string;
    }

    public int getAnzahlSpuren(){
        return spuren.size();
    }
}