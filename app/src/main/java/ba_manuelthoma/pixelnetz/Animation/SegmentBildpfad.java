package ba_manuelthoma.pixelnetz.Animation;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by thomama58961 on 29.11.2017.
 */

public class SegmentBildpfad extends MyAnimationSegment {

    private String bitmappath;

    public String getBitmappath() {
        return bitmappath;
    }
    public void setBitmappath(String bitmappath) {
        this.bitmappath = bitmappath;
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public SegmentBildpfad createFromParcel(Parcel in) {
            return new SegmentBildpfad(in);
        }
        public SegmentBildpfad[] newArray(int size) {
            return new SegmentBildpfad[size];
        }
    };


    public SegmentBildpfad(float dauer,int position,String bitmappath){//String bitmappath
        super(dauer,position);
        this.bitmappath = bitmappath;
    }

    public SegmentBildpfad(Parcel in){
        super(in);
        this.bitmappath = in.readString();
    }
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest,flags);
        dest.writeString(this.bitmappath);
    }

    @Override
    public String toString(){
        String string = super.toString();
        StringBuilder sb = new StringBuilder();
        sb.append(string);
        sb.append("\t\t\t\t\t\t\tBitmapPath: "+bitmappath+"\n");
        return sb.toString();
    }

}