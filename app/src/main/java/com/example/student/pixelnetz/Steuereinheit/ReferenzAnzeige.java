package com.example.student.pixelnetz.Steuereinheit;

import android.graphics.Bitmap;

import com.example.student.pixelnetz.Bibliothek.anzeige.InterfaceAnzeige;
import com.example.student.pixelnetz.Bibliothek.nachrichten.RegistrierungsNachricht;
import com.example.student.pixelnetz.Bibliothek.synchronisation.Zeitbasis;

import java.util.Observable;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Student on 27.10.2017.
 */

public class ReferenzAnzeige extends Observable implements InterfaceAnzeige
{
    private Pixelnetz view;
    private Timer timer;
    private Zeitbasis zeitbasis;

    public ReferenzAnzeige(Pixelnetz view, Zeitbasis zeitbasis){
        this.view = view;
        this.zeitbasis = zeitbasis;
        timer = new Timer();
    }

    @Override
    public void setTxt(RegistrierungsNachricht msg) {

    }

    @Override
    public void setAbbilden(final Bitmap bitMap, long startZeit) {

        timer.schedule(new TimerTask() {
            @Override
            public void run() {

                //view.setImgvReferenzAnzeige(bitMap);
                setChanged();
                notifyObservers(bitMap);
            }
        },startZeit-this.zeitbasis.getAktuelleUhrzeit());
    }

    @Override
    public void setBlinken(final Bitmap bitMap, final long startZeit, final long frequenz) {

        timer.schedule(new TimerTask() {
            @Override
            public void run() {

            }

        },startZeit-this.zeitbasis.getAktuelleUhrzeit());
    }

    @Override
    public void setStop(Bitmap bitMap) {
        timer.cancel();
        view.setImgvReferenzAnzeige(bitMap);
        timer = new Timer();
    }
}
