package com.example.student.pixelnetz.Bibliothek.nachrichten;

import java.io.Serializable;
import java.net.InetAddress;

/**
 * Created by Student on 02.08.2017.
 */

public class RegistrierungsNachricht implements Serializable {

    InetAddress inetAddress = null;
    int port;
    int x;
    int y;

    public RegistrierungsNachricht() {

    }
    public void setInetAddress(InetAddress inetAddress)
    {
        this.inetAddress = inetAddress;
    }
    public InetAddress getInetAddress()
    {
        return this.inetAddress;
    }
    public void setPort(int port)
    {
        this.port = port;
    }
    public int getPort()
    {
        return this.port;
    }
    public void setKennung(int x,int y)
    {
        this.x = x;
        this.y = y;
    }
    public int getX()
    {
        return this.x;
    }
    public int getY()
    {
        return this.y;
    }
}
