package com.example.student.pixelnetz.Bibliothek.kommunikationServer;

import com.example.student.pixelnetz.Bibliothek.nachrichten.AbstrakteSteuerungsNachricht;

import java.net.Socket;

/**
 * Created by David on 03.11.2017.
 */

interface InterfaceServer {

    boolean sendMessage(Socket s, AbstrakteSteuerungsNachricht m);

}
