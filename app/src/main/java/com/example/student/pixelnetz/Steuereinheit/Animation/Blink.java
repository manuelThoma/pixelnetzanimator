package com.example.student.pixelnetz.Steuereinheit.Animation;

import android.graphics.Bitmap;

import com.example.student.pixelnetz.Bibliothek.anzeige.AbstrakteAnzeige;
import com.example.student.pixelnetz.Steuereinheit.ReferenzAnzeige;

/**
 * Created by Student on 29.08.2017.
 */

public class Blink extends IAnimationsSegment
{
    private Bitmap bitMap;
    private long frequency;
    public Blink(Bitmap bitMap, long duration, AbstrakteAnzeige display, long frequency, ReferenzAnzeige referenzAnzeige)
    {
        super(duration,display,referenzAnzeige);
        this.bitMap = bitMap;
        this.frequency = frequency;
    }

    @Override
    public void send(long startTime) {
        anzeige.setBlinken(bitMap,startTime,frequency);
        referenzAnzeige.setBlinken(bitMap,startTime,frequency);
    }
}
