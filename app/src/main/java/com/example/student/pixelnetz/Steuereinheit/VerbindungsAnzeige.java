package com.example.student.pixelnetz.Steuereinheit;

import android.graphics.Color;
import android.view.Gravity;
import android.widget.GridLayout;
import android.widget.TextView;

import com.example.student.pixelnetz.Bibliothek.nachrichten.RegistrierungsNachricht;

/**
 * Created by David on 02.11.2017.
 */

public class VerbindungsAnzeige {

    private TextView[][] anzeige;
    private Pixelnetz view;
    private int groesseX;
    private int groesseY;

    public VerbindungsAnzeige(Pixelnetz view, int groesseX, int groesseY){
        this.view = view;
        this.groesseX = groesseX;
        this. groesseY = groesseY;

    }/*
    public void erzeugen(GridLayout gridLayout){

        anzeige = new TextView[groesseX][groesseY];

        gridLayout.removeAllViews();
        gridLayout.setColumnCount(groesseX);
        gridLayout.setRowCount(groesseY);
        for(int y = 0; y< groesseY; y++) {
            for (int x = 0; x < groesseX; x++) {
                anzeige[x][y] = new TextView(view);
                anzeige[x][y].setText("           ");
                anzeige[x][y].setText("0");

                GridLayout.LayoutParams param = new GridLayout.LayoutParams();
                param.height = GridLayout.LayoutParams.WRAP_CONTENT;
                param.width = GridLayout.LayoutParams.WRAP_CONTENT;
                param.rightMargin = 5;
                param.topMargin = 5;
                param.setGravity(Gravity.CENTER);
                param.columnSpec = GridLayout.spec(x);
                param.rowSpec = GridLayout.spec(y);

                anzeige[x][y].setLayoutParams(param);

                gridLayout.addView(anzeige[x][y]);
            }
        }
    }*/
/*
    public void aktualisieren(RegistrierungsNachricht nachricht){
        for(int y = 0; y< groesseY; y++) {
            for (int x = 0; x < groesseX; x++) {
                int pixel = Integer.parseInt(String.valueOf(x)+ String.valueOf(y));
                if(nachricht.getKennung() == pixel) {

                    anzeige[x][y].setText(String.valueOf(1));
                    anzeige[x][y].setTextColor(Color.BLACK);
                }

                }
            }
    }*/

    public void verbunden(int x, int y){
        anzeige[x][y].setText("1");
    }

    public void getrennt(int x, int y){
        anzeige[x][y].setText("D");
    }
}
