package com.example.student.pixelnetz.Steuereinheit;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.util.Log;

import com.example.student.pixelnetz.Steuereinheit.Animation.Animation;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * Created by Student on 23.08.2017.
 */

public class RessourcenHandler {

    private AssetManager assetManager;

    public RessourcenHandler(AssetManager assetManager)
    {
        this.assetManager = assetManager;
    }
    public Bitmap getBitmapVonAsset(String assetName) {
        InputStream istr;
        Bitmap bitmap = null;
        try
        {
            istr = assetManager.open(assetName);
            bitmap = BitmapFactory.decodeStream(istr);
            Log.i("RcourceHandler","Loading " + assetName + " was successful");
        } catch (IOException e)
        {
            // handle exception
            Log.e("RcourceHandler","Could not load asset: " + assetName);
        }

        return bitmap;
    }
    public Bitmap getAngepasstesBitmap(Bitmap bm, int newWidth, int newHeight) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // CREATE A MATRIX FOR THE MANIPULATION
        Matrix matrix = new Matrix();
        // RESIZE THE BIT MAP
        matrix.postScale(scaleWidth, scaleHeight);

        // "RECREATE" THE NEW BITMAP
        Bitmap resizedBitmap = Bitmap.createBitmap(
                bm, 0, 0, width, height, matrix, false);
        if (bm != null && !bm.isRecycled()) {
            bm.recycle();
            bm = null;
        }

        return resizedBitmap;
    }

    public void animationSpeichern(Context context, Animation animation) {


        try {
            ObjectOutputStream outputStream = new ObjectOutputStream(context.openFileOutput("animation.txt", Context.MODE_PRIVATE));
            outputStream.writeObject(animation);
            outputStream.close();
        }
        catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
    }
    public Animation animationLaden(Context context) {

        Animation ret = null;

        try {

            ObjectInputStream inStream = new ObjectInputStream(context.openFileInput("animation.txt"));

            ret = (Animation) inStream.readObject();
        }
        catch (FileNotFoundException e) {
            Log.e("login activity", "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e("login activity", "Can not read file: " + e.toString());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        return ret;
    }

}
