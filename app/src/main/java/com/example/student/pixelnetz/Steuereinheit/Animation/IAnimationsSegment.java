package com.example.student.pixelnetz.Steuereinheit.Animation;

import com.example.student.pixelnetz.Bibliothek.anzeige.AbstrakteAnzeige;
import com.example.student.pixelnetz.Steuereinheit.ReferenzAnzeige;

import java.io.Serializable;

/**
 * Created by Student on 29.08.2017.
 */

public abstract class IAnimationsSegment implements Serializable
{
    long duration;
    AbstrakteAnzeige anzeige;
    ReferenzAnzeige referenzAnzeige;

    public IAnimationsSegment(long duration, AbstrakteAnzeige anzeige, ReferenzAnzeige referenzAnzeige) {
        this.duration = duration;
        this.anzeige = anzeige;
        this.referenzAnzeige = referenzAnzeige;
    }

    public abstract void send(long startTime);

    public long getDuration()
    {
        return duration;
    }
}
