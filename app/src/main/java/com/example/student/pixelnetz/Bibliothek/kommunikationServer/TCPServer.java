/*------------------------------------------------------------------------------------------------*/
/*                                                                                                */
/*  File:   TCPServer.java                                                                        */
/*  Author: David Wolff                                                                           */
/*  Mail:   wolffda58075@th-nuernberg.de                                                          */
/*                                                                                                */
/*------------------------------------------------------------------------------------------------*/

package com.example.student.pixelnetz.Bibliothek.kommunikationServer;

/*------------------------------------------------------------------------------------------------*/
/*                                  Imports                                                       */
/*------------------------------------------------------------------------------------------------*/

import android.os.AsyncTask;
import android.util.Log;

import com.example.student.pixelnetz.Bibliothek.anzeige.AbstrakteAnzeige;
import com.example.student.pixelnetz.Bibliothek.nachrichten.AbstrakteSteuerungsNachricht;
import com.example.student.pixelnetz.Bibliothek.nachrichten.RegistrierungsNachricht;
import com.example.student.pixelnetz.Bibliothek.nachrichten.SteuerungsNachrichtPixel;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

/*------------------------------------------------------------------------------------------------*/
/*                                  Class Declaration                                             */
/*------------------------------------------------------------------------------------------------*/

/**
 * Created by David Wolff on 25.07.2017.
 */

public class TCPServer extends AsyncTask<String,SteuerungsNachrichtPixel,Boolean> implements InterfaceServer
{

    /*--------------------------------------------------------------------------------------------*/
    /*                                  Private Variable Declaration                              */
    /*--------------------------------------------------------------------------------------------*/

    private ServerSocket serverSocket = null;

    private Socket[] clients;

    private RegistrierungsNachricht registrierungsNachricht;
    private AbstrakteAnzeige pixelController;



    /*--------------------------------------------------------------------------------------------*/
    /*                                  Constructor                                               */
    /*--------------------------------------------------------------------------------------------*/

    public TCPServer(AbstrakteAnzeige pixelController, int port)
    {
        this.pixelController = pixelController;
        this.create(port);

        clients = new Socket[1000];


    }

    /*--------------------------------------------------------------------------------------------*/
    /*                                  Public Functions                                          */
    /*--------------------------------------------------------------------------------------------*/


    /*--------------------------------------------------------------------------------------------*/
    /*                                  Abstract methods of AsyncTask                             */
    /*--------------------------------------------------------------------------------------------*/

    @Override
    protected Boolean doInBackground(String... params)
    {
        int counter = 0;
        while(!isCancelled())
        {
            clients[counter] = this.acceptClient();
            registrierungsNachricht = this.receiveMessage(clients[counter]);

            String string = String.valueOf(registrierungsNachricht.getX()) + "," + String.valueOf(registrierungsNachricht.getY());
            pixelController.socketDekodierer.newItem(string,clients[counter]);
            pixelController.setTxt(registrierungsNachricht);

            counter++;

        }


        return null;
    }

    /*--------------------------------------------------------------------------------------------*/
    /*                                  Private Functions                                         */
    /*--------------------------------------------------------------------------------------------*/

    private void create(int port)
    {
        try {
            serverSocket = new ServerSocket(port);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Log.i(String.valueOf(this.getClass()), "create");
    }

    private Socket acceptClient()
    {
        try {
            Log.i(String.valueOf(this.getClass()), "acceptClient");
            return serverSocket.accept();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private RegistrierungsNachricht receiveMessage(Socket s)
    {
        ObjectInputStream inStream = null;
        RegistrierungsNachricht regMsg = null;

        Log.i(String.valueOf(this.getClass()), "receive");
        try
        {
            inStream = new ObjectInputStream(s.getInputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
        Log.i(String.valueOf(this.getClass()), "receive");

        try
        {
            regMsg = (RegistrierungsNachricht) inStream.readObject();
        } catch (IOException e)
        {
            e.printStackTrace();
        } catch (ClassNotFoundException e)
        {
            e.printStackTrace();
        }
        return regMsg;
    }

    @Override
    public boolean sendMessage(Socket s, AbstrakteSteuerungsNachricht m)
    {
        // Returns if the clients buffer item is empty
        if(s == null)
        {
            return false;
        }

        ObjectOutputStream outputStream = null;
        try {
            outputStream = new ObjectOutputStream(s.getOutputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Returns if the clients is no longer connected to the server
        if(outputStream == null)
        {
            return false;
        }

        try {
            outputStream.writeObject(m);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return true;
    }


/*------------------------------------------------------------------------------------------------*/
/*                                  End of class                                                  */
/*------------------------------------------------------------------------------------------------*/

}

