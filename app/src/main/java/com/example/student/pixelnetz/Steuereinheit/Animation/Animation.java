package com.example.student.pixelnetz.Steuereinheit.Animation;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Student on 28.08.2017.
 */

public class Animation implements Serializable
{
    private List<IAnimationsSegment> animationsSegmente;
    private long startZeitpunkt;
    private long dauer = 0;

    public Animation()
    {
        animationsSegmente = new ArrayList<>();
    }

    public void hinzufuegen(IAnimationsSegment animationsSegment)
    {
        animationsSegmente.add(animationsSegment);
    }

    public void start()
    {
        for(int i = 0; i < animationsSegmente.size(); i++)
        {
            if(i==0)
            {
                animationsSegmente.get(i).send(startZeitpunkt);
            }
            else
            {
                dauer += animationsSegmente.get(i-1).getDuration();
                animationsSegmente.get(i).send(startZeitpunkt + dauer);
            }

        }

    }

    public void setStartZeitpunkt(long startZeitpunkt){
        this.startZeitpunkt = startZeitpunkt;
    }
}
