package com.example.student.pixelnetz.Bibliothek.anzeige;

import android.graphics.Bitmap;

import com.example.student.pixelnetz.Bibliothek.nachrichten.RegistrierungsNachricht;

/**
 * Created by Student on 27.10.2017.
 */

public interface InterfaceAnzeige {

    void setTxt(RegistrierungsNachricht msg);
    void setAbbilden(Bitmap bitMap, long startZeit);
    void setBlinken(Bitmap bitMap, long startZeit, long frequenz);
    void setStop(Bitmap bitMap);
}
