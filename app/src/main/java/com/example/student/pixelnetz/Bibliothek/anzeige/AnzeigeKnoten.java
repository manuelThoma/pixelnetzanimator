package com.example.student.pixelnetz.Bibliothek.anzeige;

import android.graphics.Bitmap;
import android.util.Log;

import com.example.student.pixelnetz.Bibliothek.nachrichten.RegistrierungsNachricht;
import com.example.student.pixelnetz.Bibliothek.nachrichten.SteuerungsNachrichtKnoten;

/**
 * Created by Student on 31.08.2017.
 */

public class AnzeigeKnoten extends AbstrakteAnzeige {


    public AnzeigeKnoten(int port, int sizeX, int sizeY) {
        super(port,sizeX, sizeY);
    }

    @Override
    public void setTxt(RegistrierungsNachricht msg) {

    }

    @Override
    public void setAbbilden(Bitmap bitMap, long startZeit) {
        for(int y = 0; y< groesseY; y++)
        {
            for(int x = 0; x< groesseX; x++)
            {
                SteuerungsNachrichtKnoten message = new SteuerungsNachrichtKnoten();
                message.setStartZeit(startZeit);
                message.setBitmap(bitMap);

                // Change int array to one integer value cause hashmap can't handle array as key
                String string = String.valueOf(x) + "," + String.valueOf(y);
                if(server.sendMessage(socketDekodierer.getItem(string), message) == false)
                {
                    //txtClient1.setText( "Disconnected" );
                }
                else
                {
                    //Man kann nur einen Param übersenden deswegn der shit
                    int[] i = new int[3];
                    i[0] = x;
                    i[1] = y;

                    //setChanged();
                    //notifyObservers(i);
                }
            }
        }
        Log.i(String.valueOf(this.getClass()), "ready with sending");
    }

    @Override
    public void setBlinken(Bitmap bitMap, long startZeit, long frequenz) {

    }

    @Override
    public void setStop(Bitmap bitMap) {

    }

}
