/*------------------------------------------------------------------------------------------------*/
/*                                                                                                */
/*  File:   TCPServer.java                                                                        */
/*  Author: David Wolff                                                                           */
/*  Mail:   wolffda58075@th-nuernberg.de                                                          */
/*                                                                                                */
/*------------------------------------------------------------------------------------------------*/

package com.example.student.pixelnetz.Steuereinheit;

/*------------------------------------------------------------------------------------------------*/
/*                                  Imports                                                       */
/*------------------------------------------------------------------------------------------------*/

import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.example.student.pixelnetz.Bibliothek.synchronisation.Zeitbasis;
import com.example.student.pixelnetz.IPixelNetz;
import com.example.student.pixelnetz.Steuereinheit.Animation.Animation;
import com.example.student.pixelnetz.Steuereinheit.Animation.Show;

import java.util.Observable;
import java.util.Observer;

import ba_manuelthoma.pixelnetz.R;

/*------------------------------------------------------------------------------------------------*/
/*                                  Class Declaration                                             */
/*------------------------------------------------------------------------------------------------*/

/**
 * Created by David Wolff on 25.07.2017.
 */

public class Pixelnetz extends Observable implements IPixelNetz,Observer {//extends Observable

    /*--------------------------------------------------------------------------------------------*/
    /*                                  Private Variable Declaration                              */
    /*--------------------------------------------------------------------------------------------*/

    private Zeitbasis zeitbasis;

    private ImageView imgvReferenzAnzeige;
    //private ReferenzAnzeige[] referenzAnzeigen;

    private VirtuelleAnzeige[] virtuelleAnzeigen;

    private RessourcenHandler ressourcenHandler;
    private Animation aktuelleAnimation;

    private boolean displayAn;

    /*--------------------------------------------------------------------------------------------*/
    /*                                  API Methods                                               */
    /*--------------------------------------------------------------------------------------------*/

    public Pixelnetz(int anzahlAnzeigen)
    {
        zeitbasis = new Zeitbasis();
        zeitbasis.anfordern();

        virtuelleAnzeigen = new VirtuelleAnzeige[anzahlAnzeigen];
        aktuelleAnimation = new Animation();
    }

    @Override
    public void erstellen(int groesseX, int groesseY, int port, int id) {

        ReferenzAnzeige referenzAnzeigen = new ReferenzAnzeige(Pixelnetz.this,zeitbasis);
        Log.d("Pixelnetz","erstellen");
        virtuelleAnzeigen[id] = new VirtuelleAnzeige(groesseX, groesseY,port, referenzAnzeigen,null);
        virtuelleAnzeigen[id].getReferenzAnzeige().addObserver(this);
        virtuelleAnzeigen[id].registerObservables();
    }

    @Override
    public void anzeigen(Bitmap bitmap, long dauer, int id) {

        bitmap = getAngepasstesBitmap(bitmap, virtuelleAnzeigen[id].getGroesseX(), virtuelleAnzeigen[id].getGroesseY());
        Show show = new Show(bitmap,dauer, virtuelleAnzeigen[id].getAnzeige(), virtuelleAnzeigen[id].getReferenzAnzeige());
        aktuelleAnimation.hinzufuegen(show);
    }

    @Override
    public void aktuelleAnimationZuruecksetzen(){
        aktuelleAnimation = null;
    }

    @Override
    public void starten(long offset) {
        while(!zeitbasis.isSynchronisiert());
        long startTime = zeitbasis.getAktuelleUhrzeit()+offset;
        aktuelleAnimation.setStartZeitpunkt(startTime);
        aktuelleAnimation.start();

    }

    @Override
    public void observerHinzufuegen(Observer o) {
        addObserver(o);
    }

    public void anhalten(Bitmap bitMap,int id){

        virtuelleAnzeigen[id].getAnzeige().setStop(bitMap); // braucht auch ne Zeit um stoppzu verzögern: Nötig wenn man zwei simulationen aneinader hängin will aber die erste simulation nicht ganz zu ende laufen soll: Animation 1 -> Stopp mit sach ma mal 3 millsecunden - Starttime von simulation 2
        virtuelleAnzeigen[id].getReferenzAnzeige().setStop(bitMap);
    }

    /*--------------------------------------------------------------------------------------------*/
    /*                                  Model Init                                                */
    /*--------------------------------------------------------------------------------------------*/

    private Bitmap getAngepasstesBitmap(Bitmap bm, int newWidth, int newHeight) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // CREATE A MATRIX FOR THE MANIPULATION
        Matrix matrix = new Matrix();
        // RESIZE THE BIT MAP
        matrix.postScale(scaleWidth, scaleHeight);

        // "RECREATE" THE NEW BITMAP
        Bitmap resizedBitmap = Bitmap.createBitmap(
                bm, 0, 0, width, height, matrix, false);
        if (bm != null && !bm.isRecycled()) {
            bm.recycle();
            bm = null;
        }

        return resizedBitmap;
    }
    public void aktivierePixelRegistrierung(boolean status){
        if(status){

        }
        else{

        }
    }

    /*--------------------------------------------------------------------------------------------*/
    /*                                  Implemented methods of Observer                           */
    /*--------------------------------------------------------------------------------------------*/

    public void setImgvReferenzAnzeige(final Bitmap bitmap){
/*
        runOnUiThread(new Runnable() {
            @Override
            public void run()
            {
                imgvReferenzAnzeige.setImageBitmap(bitmap);
            }

        });*/

        displayAn = true;

    }

    @Override
    public void update(Observable observable, Object o) {
        for(int i=0; i<virtuelleAnzeigen.length; i++){
            if(observable.equals(virtuelleAnzeigen[i].getReferenzAnzeige())){

                // Verschicken von 2 Objects. Bitmap und ReferenzanzeigenID
                Object[] uebergabeWert = new Object[2];
                uebergabeWert[0] = o;
                uebergabeWert[1] = i;
                setChanged();
                notifyObservers(uebergabeWert);
                Log.d("Pixelnetz","notified");
            }
        }
    }

/*------------------------------------------------------------------------------------------------*/
/*                                  End of class                                                  */
/*------------------------------------------------------------------------------------------------*/
}
