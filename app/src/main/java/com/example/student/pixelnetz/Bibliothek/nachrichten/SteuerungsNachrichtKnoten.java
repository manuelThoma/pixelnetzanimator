package com.example.student.pixelnetz.Bibliothek.nachrichten;

import android.graphics.Bitmap;

import java.io.Serializable;

/**
 * Created by Student on 31.08.2017.
 */

public class SteuerungsNachrichtKnoten extends AbstrakteSteuerungsNachricht implements Serializable {

    private Bitmap bitmap;        // Color of Pixel
    private long startZeit;     // Set this config at given time. long is datatype of ntp

    public void setBitmap(Bitmap bitmap)
    {
        this.bitmap = bitmap;
    }
    public Bitmap getBitmap()
    {
        return this.bitmap;
    }

    public void setStartZeit(long startZeit)
    {
        this.startZeit = startZeit;
    }
    public long getStartZeit()
    {
        return this.startZeit;
    }

    public void setEnumAnzeigeStrategie(EnumAnzeigeStrategie enumAnzeigeStrategie) {

    }
    public EnumAnzeigeStrategie getEnumAnzeigeStrategie() {
        return null;
    }
}
