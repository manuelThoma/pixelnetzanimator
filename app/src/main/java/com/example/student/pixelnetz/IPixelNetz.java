package com.example.student.pixelnetz;

import android.graphics.Bitmap;

import java.util.Observer;

/**
 * Created by Student on 21.12.2017.
 */

public interface IPixelNetz
{
    /**
     * @param groesseX
     * @param groesseY
     * @param port
     * @param id    Beginnt mit id 0
     */
    void erstellen(int groesseX, int groesseY, int port, int id);
    void anzeigen(Bitmap bitmap, long dauer, int id);
    void starten(long offset);
    void observerHinzufuegen(Observer o);
    void aktuelleAnimationZuruecksetzen();
}
