package com.example.student.pixelnetz.Steuereinheit.Animation;

import android.graphics.Bitmap;

import com.example.student.pixelnetz.Bibliothek.anzeige.AbstrakteAnzeige;
import com.example.student.pixelnetz.Steuereinheit.ReferenzAnzeige;

/**
 * Created by Student on 29.08.2017.
 */

public class Show extends IAnimationsSegment {

    Bitmap bitMap;

    public Show(Bitmap bitMap, long duration, AbstrakteAnzeige display, ReferenzAnzeige referenzAnzeige)
    {
        super(duration,display,referenzAnzeige);
        this.bitMap = bitMap;
    }

    @Override
    public void send(long startTime) {
        anzeige.setAbbilden(bitMap,startTime);
        referenzAnzeige.setAbbilden(bitMap,startTime);
    }
}
