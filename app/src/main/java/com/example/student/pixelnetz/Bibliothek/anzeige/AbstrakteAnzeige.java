package com.example.student.pixelnetz.Bibliothek.anzeige;

import com.example.student.pixelnetz.Bibliothek.kommunikationServer.SocketDekodierer;
import com.example.student.pixelnetz.Bibliothek.kommunikationServer.TCPServer;

import java.util.Observable;

/**
 * Created by Student on 31.08.2017.
 */

public abstract class AbstrakteAnzeige extends Observable implements InterfaceAnzeige
{
    int port;
    int groesseX;
    int groesseY;

    TCPServer server;
    public SocketDekodierer socketDekodierer;

    boolean verbunden;

    public AbstrakteAnzeige(int port, int groesseX, int groesseY) {
        this.port = port;
        this.groesseX = groesseX;
        this.groesseY = groesseY;
    }

    public boolean isVerbunden(){
        return verbunden;
    }
}
