/**************************************************************************************************/
/*                                                                                                */
/*  File:   SocketDekodierer.java                                                                    */
/*  Author: David Wolff                                                                           */
/*  Mail:   wolffda58075@th-nuernberg.de                                                          */
/*                                                                                                */
/**************************************************************************************************/

package com.example.student.pixelnetz.Bibliothek.kommunikationServer;

/**************************************************************************************************/
/*                                  Class Declaration                                             */
/**************************************************************************************************/

import java.net.Socket;
import java.util.HashMap;

/**
 * Created by Student on 07.08.2017.
 */

public class SocketDekodierer
{
    /***********************************************************************************************/
    /*                                  Private Variable Declaration                               */
    /***********************************************************************************************/

    HashMap<String, Socket> hashMap;

    /***********************************************************************************************/
    /*                                  Constructor                                                */
    /***********************************************************************************************/

    public SocketDekodierer()
    {
        hashMap = new HashMap<String, Socket>();
    }

    public void newItem(String str, Socket s)
    {
        hashMap.put(str,s);
    }

    public Socket getItem(String str)
    {
        return hashMap.get(str);
    }
}
