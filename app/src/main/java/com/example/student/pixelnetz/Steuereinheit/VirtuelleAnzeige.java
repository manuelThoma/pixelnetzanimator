/*------------------------------------------------------------------------------------------------*/
/*                                                                                                */
/*  File:   VirtuelleAnzeige.java                                                                 */
/*  Author: David Wolff                                                                           */
/*  Mail:   wolffda58075@th-nuernberg.de                                                          */
/*                                                                                                */
/*------------------------------------------------------------------------------------------------*/

package com.example.student.pixelnetz.Steuereinheit;

/*------------------------------------------------------------------------------------------------*/
/*                                  Imports                                                       */
/*------------------------------------------------------------------------------------------------*/

import android.util.Log;

import com.example.student.pixelnetz.Bibliothek.anzeige.AbstrakteAnzeige;
import com.example.student.pixelnetz.Bibliothek.anzeige.AnzeigePixel;

import java.util.Observable;
import java.util.Observer;

/*------------------------------------------------------------------------------------------------*/
/*                                  Class Declaration                                             */
/*------------------------------------------------------------------------------------------------*/

/**
 * Created by David Wolff on 12.09.2017.
 */

public class VirtuelleAnzeige extends Observable implements Observer
{
    /*--------------------------------------------------------------------------------------------*/
    /*                                  Private Variable Declaration                              */
    /*--------------------------------------------------------------------------------------------*/

    private int groesseX;
    private int groesseY;
    private int port;

    private VerbindungsAnzeige verbindungsAnzeige;
    private ReferenzAnzeige referenzAnzeige;
    private AbstrakteAnzeige anzeige;

    public VirtuelleAnzeige(int groesseX, int groesseY, int port, ReferenzAnzeige referenzAnzeige, VerbindungsAnzeige verbindungsAnzeige) {

        this.groesseX = groesseX;
        this.groesseY = groesseY;
        this.port = port;

        this.verbindungsAnzeige = verbindungsAnzeige;
        this.referenzAnzeige = referenzAnzeige;
        //Execute on executer da nur ein thread geht
        // Nur der erste Socket geht. Lösung Selector https://stackoverflow.com/questions/5079172/java-server-multiple-ports
        anzeige = new AnzeigePixel(port,groesseX,groesseY);
    }

    public void registerObservables()
    {
        anzeige.addObserver(this);
    }

    public VerbindungsAnzeige getVerbindungsAnzeige()
    {
        return verbindungsAnzeige;
    }

    public ReferenzAnzeige getReferenzAnzeige(){ return referenzAnzeige; };

    public AbstrakteAnzeige getAnzeige()
    {
        return anzeige;
    }

    public int getGroesseX(){
        return groesseX;
    }

    public int getGroesseY(){
        return groesseY;
    }

    @Override
    public void update(Observable o, Object arg) {

        if(o instanceof AnzeigePixel)
        {
            Log.i("VirtuelleAnzeige", String.valueOf(arg));
            setChanged();
            notifyObservers(arg);
        }

    }
}
