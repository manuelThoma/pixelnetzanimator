package com.example.student.pixelnetz.Bibliothek.nachrichten;

import java.io.Serializable;

/**
 * Created by Student on 25.07.2017.
 */

public class SteuerungsNachrichtPixel extends AbstrakteSteuerungsNachricht implements Serializable {

    private int color;        // Color of Pixel
    private long startZeit;     // Set this config at given time. long is datatype of ntp
    private long frequenz;
    private EnumAnzeigeStrategie EnumAnzeigeStrategie;

    public void setColor(int color)
    {
        this.color = color;
    }
    public int getColor()
    {
        return this.color;
    }

    public void setStartZeit(long startZeit)
    {
        this.startZeit = startZeit;
    }
    public long getStartZeit()
    {
        return this.startZeit;
    }

    public void setFrequenz(long frequenz)
    {
        this.frequenz = frequenz;
    }
    public long getFrequenz()
    {
        return this.frequenz;
    }

    public void setEnumAnzeigeStrategie(EnumAnzeigeStrategie enumAnzeigeStrategie) {
        this.EnumAnzeigeStrategie = enumAnzeigeStrategie;
    }
    public EnumAnzeigeStrategie getEnumAnzeigeStrategie() {
        return EnumAnzeigeStrategie;
    }
}
