package com.example.student.pixelnetz.Bibliothek.anzeige;

import android.graphics.Bitmap;
import android.util.Log;

import com.example.student.pixelnetz.Bibliothek.kommunikationServer.SocketDekodierer;
import com.example.student.pixelnetz.Bibliothek.kommunikationServer.TCPServer;
import com.example.student.pixelnetz.Bibliothek.nachrichten.EnumAnzeigeStrategie;
import com.example.student.pixelnetz.Bibliothek.nachrichten.RegistrierungsNachricht;
import com.example.student.pixelnetz.Bibliothek.nachrichten.SteuerungsNachrichtPixel;


/**
 * Created by Student on 31.08.2017.
 */

public class AnzeigePixel extends AbstrakteAnzeige {



    public AnzeigePixel(int port, int groeseX, int groeseY) {
        super(port,groeseX, groeseY);
        server = new TCPServer(this, port);
        server.execute();
        socketDekodierer = new SocketDekodierer();
    }

    public void setTxt(RegistrierungsNachricht msg){
        Log.i("PixelControler", String.valueOf(msg));
        /*
        int[] i = new int[2];
        i[0] = x;
        i[1] = y;
        setChanged();
        notifyObservers(i);*/ //TODO: das ist noch falsch. Pixelerkennung funktioniert nicht richtig.
        setChanged();
        notifyObservers(msg);
    }

    @Override
    public void setAbbilden(Bitmap bitMap, long startZeit)
    {
        Log.i(String.valueOf(this.getClass()), "start sending");
        for(int y = 0; y< groesseY; y++)
        {
            for(int x = 0; x< groesseX; x++)
            {
                SteuerungsNachrichtPixel message = new SteuerungsNachrichtPixel();
                message.setStartZeit(startZeit);
                message.setColor(bitMap.getPixel(x,y));
                message.setEnumAnzeigeStrategie(EnumAnzeigeStrategie.ABBILDEN);
                Log.d("AnzeigePixel","setAbbilden");
                // Change int array to one integer value cause hashmap can't handle array as key
                String string = String.valueOf(x) + "," + String.valueOf(y);

                if(server.sendMessage(socketDekodierer.getItem(string), message) == false)
                {
                    verbunden = false;
                    int[] i = new int[2];
                    i[0] = x;
                    i[1] = y;
                    setChanged();
                    notifyObservers(i);
                }
                else
                {
                    //Man kann nur einen Param übersenden deswegn der shit
                    verbunden = true;
                    int[] i = new int[2];
                    i[0] = x;
                    i[1] = y;
                    setChanged();
                    notifyObservers(i);
                }
            }
        }
        Log.i(String.valueOf(this.getClass()), "ready with sending");
    }

    @Override
    public void setBlinken(Bitmap bitMap, long startZeit, long frequenz) {
        Log.i(String.valueOf(this.getClass()), "start sending");
        for(int y = 0; y< groesseY; y++)
        {
            for(int x = 0; x< groesseX; x++)
            {
                SteuerungsNachrichtPixel message = new SteuerungsNachrichtPixel();
                message.setStartZeit(startZeit);
                message.setColor(bitMap.getPixel(x,y));
                message.setEnumAnzeigeStrategie(EnumAnzeigeStrategie.BLINKEN);
                message.setFrequenz(frequenz);

                // Change int array to one integer value cause hashmap can't handle array as key
                String string = String.valueOf(x) + "," + String.valueOf(y);
                if(server.sendMessage(socketDekodierer.getItem(string), message) == false)
                {
                    setChanged();
                    notifyObservers(string);
                }
                else
                {   //TODO: Wird nicht benutzt
                    //Man kann nur einen Param übersenden deswegn der shit
                    int[] i = new int[3];
                    i[0] = x;
                    i[1] = y;
                    i[2] = message.getColor();

                    setChanged();
                    notifyObservers(i);
                }
            }
        }
        Log.i(String.valueOf(this.getClass()), "ready with sending");
    }

    @Override
    public void setStop(Bitmap bitMap) {
        Log.i(String.valueOf(this.getClass()), "start sending");
        for(int y = 0; y< groesseY; y++)
        {
            for(int x = 0; x< groesseX; x++)
            {
                SteuerungsNachrichtPixel message = new SteuerungsNachrichtPixel();
                message.setColor(bitMap.getPixel(x,y));
                message.setEnumAnzeigeStrategie(EnumAnzeigeStrategie.STOP);


                String string = String.valueOf(x) + "," + String.valueOf(y);
                if(server.sendMessage(socketDekodierer.getItem(string), message) == false)
                {
                    //txtClient1.setText( "Disconnected" );
                }
                else
                {
                    //Man kann nur einen Param übersenden deswegn der shit
                    int[] i = new int[3];
                    i[0] = x;
                    i[1] = y;
                    i[2] = message.getColor();

                    //setChanged();
                    //notifyObservers(i);
                }
            }
        }
        Log.i(String.valueOf(this.getClass()), "ready with sending");
    }


}
